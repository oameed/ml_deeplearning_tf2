# Deep Learning with [TensorFlow](https://www.tensorflow.org/) 

## TensorFlow [\[Blog\]](https://blog.tensorflow.org/)[\[GitHub\]](https://github.com/tensorflow/tensorflow)

### Tutorials

[[view].](https://www.tensorflow.org/overview) TensorFlow Overview: Tutorials & Guides  
[[view].](https://keras.io/examples/) Keras Code Examples  
[[view].](https://github.com/tensorflow/models) TensorFlow Model Garden  

### Trained Model Repositories

[[view].](https://www.tensorflow.org/hub) TensorFlow Hub  

### Most Used Modules/APIs

**tf**
 * [tf.GradientTape](https://www.tensorflow.org/api_docs/python/tf/GradientTape)  

**tf.data**
 * [tf.data.TFRecordDataset](https://www.tensorflow.org/api_docs/python/tf/data/TFRecordDataset)  

**tf.io**
 * [tf.io.FixedLenFeature](https://www.tensorflow.org/api_docs/python/tf/io/FixedLenFeature)  
 * [tf.io.TFRecordWriter](https://www.tensorflow.org/api_docs/python/tf/io/TFRecordWriter)  
 * [tf.io.decode_raw](https://www.tensorflow.org/api_docs/python/tf/io/decode_raw)  
 * [tf.io.parse_single_example](https://www.tensorflow.org/api_docs/python/tf/io/parse_single_example)  

**tf.keras**
 * [tf.keras.Input](https://www.tensorflow.org/api_docs/python/tf/keras/Input)  
 * [tf.keras.Model](https://www.tensorflow.org/api_docs/python/tf/keras/Model)  
 * tf.keras.callbacks
   * [tf.keras.callbacks.Callback](https://www.tensorflow.org/api_docs/python/tf/keras/callbacks/Callback)  
   * [tf.keras.callbacks.TensorBoard](https://www.tensorflow.org/api_docs/python/tf/keras/callbacks/TensorBoard)  
 * [tf.keras.datasets](https://www.tensorflow.org/api_docs/python/tf/keras/datasets)  
 * [tf.keras.layers](https://www.tensorflow.org/api_docs/python/tf/keras/layers)  
   * [tf.keras.layers.Layer](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Layer)
 * [tf.keras.losses](https://www.tensorflow.org/api_docs/python/tf/keras/losses)  
   * [tf.keras.losses.Loss](https://www.tensorflow.org/api_docs/python/tf/keras/losses/Loss)  
 * [tf.keras.metrics](https://www.tensorflow.org/api_docs/python/tf/keras/metrics)  
 * tf.keras.models
   * [tf.keras.models.load_model](https://www.tensorflow.org/api_docs/python/tf/keras/models/load_model)  
   * [tf.keras.models.save_model](https://www.tensorflow.org/api_docs/python/tf/keras/models/save_model)  
 * [tf.keras.optimizers](https://www.tensorflow.org/api_docs/python/tf/keras/optimizers)  

**tf.summary**
 * [tf.summary.create_file_writer](https://www.tensorflow.org/api_docs/python/tf/summary/create_file_writer)  
 * [tf.summary.image](https://www.tensorflow.org/api_docs/python/tf/summary/image)  

**tf.train**
 * [tf.train.BytesList](https://www.tensorflow.org/api_docs/python/tf/train/BytesList)  
 * [tf.train.Features](https://www.tensorflow.org/api_docs/python/tf/train/Features)  
 * [tf.train.Feature](https://www.tensorflow.org/api_docs/python/tf/train/Feature)  
 * [tf.train.Int64List](https://www.tensorflow.org/api_docs/python/tf/train/Int64List)  

## Useful

[[U 1 ].](https://fleuret.org/francois/) 2023. Fleuret. _The Little Book of Deep Learning_  
[[U 2 ].](https://towardsdatascience.com/kaiming-he-initialization-in-neural-networks-math-proof-73b9a0d845c4) 2023. Hlav. Kaiming He Initialization in Neural Networks — Math Proof  
[[U 3 ].](https://github.com/google-research/tuning_playbook) 2023. Google. Deep Learning Tuning Playbook  
[[U 4 ].](https://towardsdatascience.com/xavier-glorot-initialization-in-neural-networks-math-proof-4682bf5c6ec3) 2022. Hlav. Xavier Glorot Initialization in Neural Networks — Math Proof  
[[U 5 ].](https://ai.stackexchange.com/questions/29904/what-is-lipschitz-constraint-and-why-it-is-enforced-on-discriminator) 2021. hanugm. What is Lipschitz constraint and why it is enforced on discriminator?  
[[U 6 ].](https://www.youtube.com/watch?v=w8yWXqWQYmU) 2020. Zhang. _Building a neural network FROM SCRATCH_  
[[U 7 ].](https://machinelearningmastery.com/how-to-code-a-wasserstein-generative-adversarial-network-wgan-from-scratch/) 2019. Brownlee. How to Develop a Wasserstein GAN (WGAN) From Scratch  
[[U 8 ].](https://machinelearningmastery.com/practical-guide-to-gan-failure-modes/) 2019. Brownlee. How to Identify and Diagnose GAN Failure Modes  
[[U 9 ].](https://machinelearningmastery.com/how-to-develop-a-conditional-generative-adversarial-network-from-scratch/) 2019. Brownlee. How to Develop a Conditional GAN (cGAN) From Scratch   
[[U10].](https://machinelearningmastery.com/how-to-develop-a-generative-adversarial-network-for-an-mnist-handwritten-digits-from-scratch-in-keras/) 2019. Brownlee. How to Develop a GAN for Generating MNIST  
[[U11].](https://machinelearningmastery.com/how-to-code-generative-adversarial-network-hacks/) 2019. Brownlee. How to Implement GAN Hacks in Keras to Train Stable Models  
[[U12].](https://arxiv.org/abs/1904.08994) 2019. Weng. From GAN to WGAN [\[Blog\]](https://lilianweng.github.io/posts/2017-08-20-gan/)  
[[U13].](https://jalammar.github.io/visualizing-neural-machine-translation-mechanics-of-seq2seq-models-with-attention/) 2018. Alammar. Visualizing A Neural Machine Translation Model  
[[U14].](http://jalammar.github.io/illustrated-transformer/) 2018. Alammar. The Illustrated Transformer  
[[U15].](http://nlp.seas.harvard.edu/2018/04/03/attention.html) 2018. Harvard NLP. The Annotated Transformer  
[[U16].](https://colah.github.io/posts/2015-08-Understanding-LSTMs/) 2015. Olah. Understanding LSTM Networks  
[[U17].](http://colah.github.io/posts/2014-07-NLP-RNNs-Representations/) 2014. Olah. Deep Learning, NLP, and Representations  
[[U18].](https://www.coursera.org/lecture/build-basic-generative-adversarial-networks-gans/welcome-to-week-3-veMjm) 0000. Zhou. Wasserstein GANs with Gradient Penalty: Week 3 of Coursera's Build Basic GANs  

## Acknowledgements

* We would like to thank the University of New Mexico Center for Advanced Research Computing [(CARC)](http://carc.unm.edu/), supported in part by the National Science Foundation, for providing the high performance computing resources used in this work.

* The Transformer code used in this project is an adaptation from TensorFlow's tutorial on Transformers: [_Neural machine translation with a Transformer and Keras_](https://www.tensorflow.org/text/tutorials/transformer). 

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.90  T=0.05 s (613.3 files/s, 67017.7 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                          16            292            219           1848
Markdown                         1             61              0            267
Bourne Shell                    11             87             35            195
MATLAB                           1             12             10            143
-------------------------------------------------------------------------------
SUM:                            29            452            264           2453
-------------------------------------------------------------------------------
</pre>

## How to Run

* This project (except the Transformer) uses `Tensorflow` Version `2.2` along with `cudatoolkit` Version `10.1.243` and `cudnn` Version `7.6.5`. The `YAML` files for creating the conda environments used to run this project are included in `run/conda`.  
* The Transformer uses `Tensorflow` Version `2.11`.  
* _**Notes:**_  
  * `numpy` must be restricted to Version `1.18.5` to prevent errors of type [`cannot convert symbolic tensor to numpy array`](https://stackoverflow.com/questions/66207609/notimplementederror-cannot-convert-a-symbolic-tensor-lstm-2-strided-slice0-t).  
  * To successfully run on GPU, `cudatoolkit` and `cudnn` must be installed in the environment before installing Tensorflow as suggested in [Install TensorFlow with pip](https://www.tensorflow.org/install/pip).   
  * `CUDA` and `cudnn` compatibility requirements for the desired TensorFlow version can be looked up from [TensorFlow Build From Source](https://www.tensorflow.org/install/source#gpu).  
  * To confirm successful operation on GPU, run the snippets suggested in TensorFlow's [Use a GPU: Setup & Logging device placement](https://www.tensorflow.org/guide/gpu#setup).  
  * MNIST dataset must be downloaded manually and placed in `data/mnist/` and [python-mnist](https://pypi.org/project/python-mnist/) package must be installed.  
* To run experiments (with training a classifier as an example):
  * on Local PC:
    1. `cd` to main project directory
    2. `./run/sh/train_v11.sh`
  * on HPC system:
    1. `cd` to main project directory
    2. `cd ./run/slrm/`
    3. `sbatch train_v1x.sh`
  * use _Tensorboard_ for monitoring (with training v11 as an example):
    1. `cd` to main project directory
    2. `cd networks/v11/`
    3. `tensorboard --logdir logs/train/`

## Datasets
|     |     |     |
|:---:|:---:|:---:|
**[MNIST](http://yann.lecun.com/exdb/mnist/)** | **[FashionMNIST](https://github.com/zalandoresearch/fashion-mnist)** | **[CIFAR-10](https://www.cs.toronto.edu/~kriz/cifar.html)**
![][mnist] | ![][fashionmnist] | ![][cifar10]

[mnist]:data/mnist/samples/mnist_samples.png
[fashionmnist]:data/fashionmnist/samples/fashionmnist_samples.png
[cifar10]:data/cifar10/samples/cifar10_samples.png

**[MovingMNIST](http://www.cs.toronto.edu/~nitish/unsupervised_video/)**
|     |     |     |     |     |     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
![][MMNIST_0] | ![][MMNIST_1] | ![][MMNIST_2] | ![][MMNIST_3] | ![][MMNIST_4] | ![][MMNIST_5] | ![][MMNIST_6] | ![][MMNIST_7] | ![][MMNIST_8] | ![][MMNIST_9]

[MMNIST_0]:data/movingmnist/samples/sample_164.gif
[MMNIST_1]:data/movingmnist/samples/sample_574.gif
[MMNIST_2]:data/movingmnist/samples/sample_972.gif
[MMNIST_3]:data/movingmnist/samples/sample_1176.gif
[MMNIST_4]:data/movingmnist/samples/sample_2803.gif
[MMNIST_5]:data/movingmnist/samples/sample_3054.gif
[MMNIST_6]:data/movingmnist/samples/sample_3743.gif
[MMNIST_7]:data/movingmnist/samples/sample_4636.gif
[MMNIST_8]:data/movingmnist/samples/sample_6125.gif
[MMNIST_9]:data/movingmnist/samples/sample_7402.gif

**[Portuguese to English Dataset](https://www.tensorflow.org/datasets/catalog/ted_hrlr_translate#ted_hrlr_translatept_to_en)**
|     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|
![][PED1] | ![][PED2] |![][PED3] |![][PED4] |![][PED5]  |
![][PED6] | ![][PED7] |![][PED8] |![][PED9] |![][PED10] |

[PED1]:data/pt-en-TED/samples/pt-en-TED_samples_1.png
[PED2]:data/pt-en-TED/samples/pt-en-TED_samples_2.png
[PED3]:data/pt-en-TED/samples/pt-en-TED_samples_3.png
[PED4]:data/pt-en-TED/samples/pt-en-TED_samples_4.png
[PED5]:data/pt-en-TED/samples/pt-en-TED_samples_5.png
[PED6]:data/pt-en-TED/samples/pt-en-TED_samples_6.png
[PED7]:data/pt-en-TED/samples/pt-en-TED_samples_7.png
[PED8]:data/pt-en-TED/samples/pt-en-TED_samples_8.png
[PED9]:data/pt-en-TED/samples/pt-en-TED_samples_9.png
[PED10]:data/pt-en-TED/samples/pt-en-TED_samples_10.png

## Experiments 

### v1x: Classifier

|     |     |     |
|:---:|:---:|:---:|
**MNIST**   |**FashionMNIST**| **CIFAR-10**
![][v11_cm] | ![][v12_cm]    | ![][v13_cm]
[\[training metrics\]](networks/v11/logs/train/metrics.png) |[\[training metrics\]](networks/v12/logs/train/metrics.png) |[\[training metrics\]](networks/v13/logs/train/metrics.png)

[v11_cm]:networks/v11/predictions/predictions.png
[v12_cm]:networks/v12/predictions/predictions.png
[v13_cm]:networks/v13/predictions/predictions.png

### v2x: Auto-Encoder

|     |     |     |
|:---:|:---:|:---:|
**MNIST**     |**FashionMNIST**| **CIFAR-10**
![][v21_pred] | ![][v22_pred]  | ![][v23_pred]
[\[training metrics\]](networks/v21/logs/train/metrics.png) | [\[training metrics\]](networks/v22/logs/train/metrics.png) | [\[training metrics\]](networks/v23/logs/train/metrics.png)

[v21_pred]:networks/v21/predictions/predictions.png
[v22_pred]:networks/v22/predictions/predictions.png
[v23_pred]:networks/v23/predictions/predictions.png

### v3x: Encoder-Reccurent-Decoder

_training progress_

|     |
|:---:|
![][TMMNIST]

[TMMNIST]:networks/v31/logs/train/training.gif

_Predictions_

|     |     |     |     |     |     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
![][PMMNIST_0] | ![][PMMNIST_1] | ![][PMMNIST_2] | ![][PMMNIST_3] | ![][PMMNIST_4] | ![][PMMNIST_5] | ![][PMMNIST_6] | ![][PMMNIST_7] | ![][PMMNIST_8] | ![][PMMNIST_9]

[PMMNIST_0]:networks/v31/predictions/prediction_1.gif
[PMMNIST_1]:networks/v31/predictions/prediction_2.gif
[PMMNIST_2]:networks/v31/predictions/prediction_3.gif
[PMMNIST_3]:networks/v31/predictions/prediction_4.gif
[PMMNIST_4]:networks/v31/predictions/prediction_5.gif
[PMMNIST_5]:networks/v31/predictions/prediction_6.gif
[PMMNIST_6]:networks/v31/predictions/prediction_7.gif
[PMMNIST_7]:networks/v31/predictions/prediction_8.gif
[PMMNIST_8]:networks/v31/predictions/prediction_9.gif
[PMMNIST_9]:networks/v31/predictions/prediction_10.gif

[\[training metrics\]](networks/v31/logs/train/metrics.png)

### v4x: Generative Adversarial Network (GAN)

Wasserstein GAN with Gradient Penalty

|     |     |     |
|:---:|:---:|:---:|
**MNIST**     |**FashionMNIST**| **CIFAR-10**
![][Tv41]     | ![][Tv42]      | ![][Tv43]
![][v41_pred] | ![][v42_pred]  | ![][v43_pred]
![][v41_m   ] | ![][v42_m   ]  | ![][v43_m   ]

[Tv41]:networks/v41/logs/train/training.gif
[Tv42]:networks/v42/logs/train/training.gif
[Tv43]:networks/v43/logs/train/training.gif
[v41_pred]:networks/v41/predictions/predictions.png
[v42_pred]:networks/v42/predictions/predictions.png
[v43_pred]:networks/v43/predictions/predictions.png
[v41_m]:networks/v41/logs/train/metrics.png
[v42_m]:networks/v42/logs/train/metrics.png
[v43_m]:networks/v43/logs/train/metrics.png

### v51: Transformer

|     |     |     |
|:---:|:---:|:---:|
**__Positional Encoding of Inputs__** | **__Positional Encoding of Targets__** | **__Learning Rate__**
![][v51_1]                            | ![][v51_2]                             | ![][v51_3]            |

[v51_1]:networks/v51/positional_encoding_input.png
[v51_2]:networks/v51/positional_encoding_target.png
[v51_3]:networks/v51/learning_rate.png

|     |     |
|:---:|:---:|
**_Training Loss_** | **_Training Accuracy_**
![][v51_8]          | ![][v51_9] 

[v51_8]:networks/v51/logs/train/metrics_loss.png
[v51_9]:networks/v51/logs/train/metrics_accuracy.png

|     |     |
|:---:|:---:|
**__Predictions__** | **__Attention Plots__**
![][v51_4_1]        | ![][v51_4_2]           |  
![][v51_5_1]        | ![][v51_5_2]           |
![][v51_6_1]        | ![][v51_6_2]           |
![][v51_7_1]        | ![][v51_7_2]           |

[v51_4_1]:networks/v51/predictions/predictions_1_txt.png
[v51_4_2]:networks/v51/predictions/predictions_1_attw.png
[v51_5_1]:networks/v51/predictions/predictions_2_txt.png
[v51_5_2]:networks/v51/predictions/predictions_2_attw.png
[v51_6_1]:networks/v51/predictions/predictions_3_txt.png
[v51_6_2]:networks/v51/predictions/predictions_3_attw.png
[v51_7_1]:networks/v51/predictions/predictions_4_txt.png
[v51_7_2]:networks/v51/predictions/predictions_4_attw.png



