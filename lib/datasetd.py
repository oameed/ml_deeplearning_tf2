#######################################
### DEEP LEARNING WITH TENSORFLOW 2 ###
### DATASET FUNCTION DEFINITIONS    ###
### by: OAMEED NOAKOASTEEN          ###
#######################################

import os
import sys
import numpy as np
from utilsd import (getFILENAMES  , 
                    tar_uncompress, 
                    pickle_read   ,
                    plotter       ,
                    wHDF          ,
                    download      ,
                    clone         ,
                    mDIR          ,
                    rDIR           )

def dataset_get_mnist(PATH,MODE):
 urls_names=[['http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz','train-images-idx3-ubyte.gz'],
             ['http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz','train-labels-idx1-ubyte.gz'],
             ['http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz' ,'t10k-images-idx3-ubyte.gz' ],
             ['http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz' ,'t10k-labels-idx1-ubyte.gz' ] ] 
 if False:                                                                               # MNIST MUST BE DOWNLOADED MANUALLY
  for url, name in urls_names:
   if not  name in getFILENAMES(PATH,False):
    print(' DOWNLOADING '+name+' ... ')
    download(url,os.path.join(PATH,name))
 from mnist import MNIST
 mndata    =MNIST(PATH)
 mndata.gz =True
 if  MODE  =='train':
  img,lab  =mndata.load_training()
 else:
  if MODE  =='test' :
   img,lab =mndata.load_testing ()
 img       =np.reshape(np.array(img),[-1,28,28,1])
 lab       =np.reshape(np.array(lab),[-1        ])    
 return img,lab

def dataset_get_fashionmnist(PATH,MODE):
 urls_names=[['https://github.com/zalandoresearch/fashion-mnist','fashion-mnist']]
 for url, name in urls_names:
  if not  name in getFILENAMES(PATH,False):
   print(' DOWNLOADING '+name+' ... ')
   clone(url,PATH,name)
 if MODE=='test':
  MODE='t10k'
 sys.path.append      (os.path.join(sys.path[0],PATH,'fashion-mnist','utils'         ))
 datapath=os.path.join(                         PATH,'fashion-mnist','data','fashion')
 import mnist_reader
 img,lab =mnist_reader.load_mnist(datapath, kind=MODE)
 img     =np.reshape(img,[-1,28,28,1])
 lab     =np.reshape(lab,[-1        ])
 return img,lab

def dataset_get_cifar10(PATH,MODE):
 urls_names=[['https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz','cifar-10-python.tar.gz']] 
 for url, name in urls_names:
  if not  name in getFILENAMES(PATH,False):
   print(' DOWNLOADING '+name+' ... ')
   download(url,os.path.join(PATH,name))
 LIST_train=['data_batch_1',
             'data_batch_2',
             'data_batch_3',
             'data_batch_4',
             'data_batch_5' ]
 LIST_test =['test_batch'   ]
 if  MODE=='train':
  LIST =LIST_train
 else:
  if MODE=='test' :
   LIST=LIST_test
 img=[]
 lab=[]
 dirtemp=os.path.join(PATH,'uncompressed')
 mDIR(dirtemp)
 tar_uncompress        (os.path.join(PATH,urls_names[0][1]),dirtemp)
 for i in range(len(LIST)):
  filename=os.path.join(dirtemp,getFILENAMES(dirtemp, False)[0],LIST[i])
  DICT    =pickle_read(filename)
  img.append(         DICT[b'data'  ] )
  lab.append(np.array(DICT[b'labels']))
 rDIR(dirtemp)
 img=np.reshape(np.array(img),[-1,3,32,32])
 lab=np.reshape(np.array(lab),[-1        ])
 img=img.transpose((0,2,3,1))    
 return img,lab

def dataset_get_movingmnist(PATH,MODE):
 urls_names=[['http://www.cs.toronto.edu/~nitish/unsupervised_video/mnist_test_seq.npy','mnist_test_seq.npy']]
 for url, name in urls_names:
  if not  name in getFILENAMES(PATH,False):
   print(' DOWNLOADING '+name+' ... ')
   download(url,os.path.join(PATH,name))
 img     =np.load(os.path.join(PATH,urls_names[0][1]))
 img     =img.transpose((1,0,2,3))
 img     =np.reshape(img,[-1,20,64,64,1])
 if MODE =='train':
  img    =img[0              :int(0.75*10000)]
 else:
  if MODE=='test' :
   img   =img[int(0.75*10000):               ]
 lab     =np.random.randint(0,10,size=(img.shape[0]))                                    # DUMMY LABELS
 return img,lab

def dataset_get_ted_hrlr_translate_pt_to_en(PATH,MODE):
 import tensorflow_datasets as tfds
 examples,metadata= tfds.load('ted_hrlr_translate/pt_to_en', 
                             with_info    =True            ,
                             as_supervised=True             )
 return examples[MODE],[]

def dataset_get(TYPE,PATH,MODE):
 if     TYPE=='type1':
  img,lab    =dataset_get_mnist                      (PATH,MODE)
 else:
  if    TYPE=='type2':
   img,lab   =dataset_get_fashionmnist               (PATH,MODE)
  else:
   if   TYPE=='type3':
    img,lab  =dataset_get_cifar10                    (PATH,MODE)
   else:
    if  TYPE=='type4':
     img,lab =dataset_get_movingmnist                (PATH,MODE)
    else:
     if TYPE=='type5':
      img,lab=dataset_get_ted_hrlr_translate_pt_to_en(PATH,MODE)
 return img,lab

def dataset_sample(PATHS,PARAMS):
 from matplotlib import pyplot as plt
 def get_class_indeces(LAB,CLASS):
  return np.sort(np.random.choice([i for i,j in enumerate(LAB) if LAB[i]==CLASS],size=10)).tolist()
 def get_plot_images(IMG,LAB):
  img =[]
  for i in range(10):
   img.append(IMG[get_class_indeces(LAB,i)])
  img=np.concatenate(img)
  return img
 def get_plot_texts(IMG):
  #IDX=np.sort(np.random.randint(0,len(IMG),10))
  IDX=[2279,5143,9858,18033,21712,25918,34703,39779,49557,50878]
  img=[]
  for  idx,(pt_examples,en_examples) in enumerate(IMG):
   if idx in IDX:
    img.append((pt_examples.numpy().decode('utf-8'),en_examples.numpy().decode('utf-8')))
  return img
 img,lab    =dataset_get (PARAMS[0][0], PATHS [0]   ,    'train'  )
 filename   =os.path.join(PATHS [1]   , PARAMS[1][0]+'_'+'samples')
 if     PARAMS[0][0]=='type1' :
  img                =get_plot_images(img, lab)
  img                =255-img
  colormap           ='gray'
 else:
  if    PARAMS[0][0]=='type2' :
   img               =get_plot_images(img, lab)
   img               =255-img
   colormap          ='gray'
  else:
   if   PARAMS[0][0]=='type3' :
    img              =get_plot_images(img, lab)
    colormap         ='viridis'
   else:
    if  PARAMS[0][0]=='type4' :
     #idx           =np.sort(np.random.randint(0,img.shape[0],10))
     idx             =[ 164,  574,  972, 1176, 2803, 3054, 3743, 4636, 6125, 7402]
    else:
     if PARAMS[0][0]=='type5' :
      img            =get_plot_texts(img)
 ### PLOT / EXPORT FOR GIF GENERATION
 if     PARAMS[0][0] in ['type1','type2','type3']:
  fig=plotter('collage', img,[colormap])
  plt.savefig(filename+'.png', format='png', transparent=True)
 else:
  if    PARAMS[0][0] in ['type4'                ]:   
   wHDF(filename+'.h5'         ,
        [str(e)   for e in idx],
        [img[e]   for e in idx] )
  else:
   if   PARAMS[0][0] in ['type5'                ]:
    for i in range(len(img)):
     fig=plotter('txt', img[i],[])
     plt.savefig(filename+'_'+str(i+1)+'.png', format='png', transparent=True)


