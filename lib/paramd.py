#######################################
### DEEP LEARNING WITH TENSORFLOW 2 ###
### PARAMETER DEFINITIONS           ###
### by: OAMEED NOAKOASTEEN          ###
#######################################

import os
import argparse

### DEFINE PARSER
parser=argparse.ArgumentParser() 

### DEFINE PARAMETERS
parser.add_argument('-data', type=str  ,                help='NAME-OF-DATA-TYPE-DIRECTORY',required=True       )
parser.add_argument('-net' , type=str  , default='v00', help='NAME-OF-NETWORK-PROJECT'                         )
parser.add_argument('-sc'  , type=int  , default=1    , help='DATA SCALING SCHEME '                            )
parser.add_argument('-pt'  , type=int  , default=0    , help='PROBLEM TYPE'                                    )
parser.add_argument('-cpu' ,                            help='PLACE ON CPU'               , action='store_true')
  # GROUP: HYPER-PARAMETERS FOR TRAINING
parser.add_argument('-b'   , type=int  , default=128  , help='BATCH-SIZE'                                      )
parser.add_argument('-epc' , type=int  , default=50   , help='NUMBER-OF-TRAINING-EPOCHS'                       )
parser.add_argument('-bc'  , type=int  , default=10000, help='BATCH-BUFFER-CAPACITY'                           )
parser.add_argument('-step', type=int  , default=200  , help='STEPPING OPTION'                                 )
parser.add_argument('-vs'  , type=float, default=0.1  , help='VALIDATION SPLIT '                               )
 # GROUP:  HYPER-PARAMETERS FOR ARCHITECTURES
parser.add_argument('-zd'  , type=int  , default=100  , help='GAN LATENT DIMENSION'                            )

### ENABLE FLAGS
args=parser.parse_args()

### CONSTRUCT PARAMETER STRUCTURES

datatypes={"type1":"mnist"       ,
           "type2":"fashionmnist",
           "type3":"cifar10"     ,
           "type4":"movingmnist" ,
           "type5":"pt-en-TED"    }

if not args.cpu:
 device  = 'GPU'
else:
 device  = 'CPU'           
           
PATHS    =[os.path.join('..','..','data'    ,datatypes[args.data]               ),
           os.path.join('..','..','data'    ,datatypes[args.data],'samples'     ),
           os.path.join('..','..','networks',args.net            ,'tfrecords'   ),
           os.path.join('..','..','networks',args.net            ,'checkpoints' ),
           os.path.join('..','..','networks',args.net            ,'logs'        ),
           os.path.join('..','..','networks',args.net            ,'predictions' ) ]

PARAMS   =[[args.data           ,
            args.net            ,
            args.sc             ,
            args.b              ,
            args.epc            ,
            args.bc             ,
            args.step           ,
            args.zd             ,
            args.vs             ,
            args.pt             ,
            device               ]]

if     args.data=='type1':
 PARAMS.append   (["mnist"]) 
 PARAMS.append   ([28,28,1])
 PARAMS.append   (['int64','uint8'])
 PARAMS.append   (['0','1','2','3','4','5','6','7','8','9'])
else:
 if    args.data=='type2':
  PARAMS.append  (["fashionmnist"])
  PARAMS.append  ([28,28,1])
  PARAMS.append  (['uint8','uint8'])
  PARAMS.append  (['T-shirt/Top','Trouser','Pullover','Dress','Coat','Sandal','Shirt','Sneaker','Bag','Ankle Boot'])
 else:
  if   args.data=='type3':
   PARAMS.append (["cifar10"])
   PARAMS.append ([32,32,3])
   PARAMS.append (['uint8','int64'])
   PARAMS.append (['airplane','automobile','bird','cat','deer','dog','frog','horse','ship','truck'])
  else:
   if  args.data=='type4':
    PARAMS.append(["movingmnist"])
    PARAMS.append([20,64,64,1])
    PARAMS.append(['uint8' ,[]  ])
    PARAMS.append([])
   else:
    if args.data=='type5':
     PARAMS.append(["pt-en-TED"])
     PARAMS.append([])
     PARAMS.append(['utf-8','utf-8'])
     PARAMS.append([])
     PARAMS.append([2,128,512,8,0.1,7765,7010,1000,1000])
 
# PARAMS        [0][0] : DATASET TYPE
# PARAMS        [0][1] : NETWORK DIRECTORY
# PARAMS        [0][2] : DATA SCALING SCHEME: '1': SCALE TO [0,1], '2': SCALE TO [-1,1]
# PARAMS        [0][3] : BATCH SIZE
# PARAMS        [0][4] : TRAINING EPOCHS
# PARAMS        [0][5] : BATCH BUFFER CAPACITY
# PARAMS        [0][6] : STEPPING OPTION
# PARAMS        [0][7] : GAN's LATENT DIMENSION
# PARAMS        [0][8] : VALIDATION SPLIT RATIO
# PARAMS        [0][9] : PROBLEM TYPE: '1': classifier, '2': autoencoder, '3': rnn, '4': gan, '5':transformer
# PARAMS        [0][10]: DEVICE TYPE: DEFAULT IS GPU, IF FLAG IS PROVIDED, SWITCHES TO CPU 

# PARAMS        [1][0] : DATASET NAME

# PARAMS        [2][*] : DATASET SHAPE

# PARAMS        [3][*] : DATASET DATA TYPES

# PARAMS        [4][*] : DATASET CLASSES

# PARAMS        [5][0] : TRANSFORMER: NUMBER OF LAYERS
# PARAMS        [5][1] : TRANSFORMER: DEPTH  OF THE MODEL
# PARAMS        [5][2] : TRANSFORMER: DEPTH  OF THE FEED FORWARD NETWORK 
# PARAMS        [5][3] : TRANSFORMER: NUMBER OF HEADS
# PARAMS        [5][4] : TRANSFORMER: DROPOUT RATE
# PARAMS        [5][5] : TRANSFORMER: INPUT  (PT) VOCAB SIZE
# PARAMS        [5][6] : TRANSFORMER: TARGET (EN) VOCAB SIZE
# PARAMS        [5][7] : TRANSFORMER: INPUT  MAXIMUM POSITIONAL ENCODING
# PARAMS        [5][8] : TRANSFORMER: OUTPUT MAXIMUM POSITIONAL ENCODING


