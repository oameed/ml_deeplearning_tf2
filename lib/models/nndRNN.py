###############################################
### DEEP LEARNING WITH TENSORFLOW 2         ###
### TRAIN ENCODER-RECURRENT-DECODER NETWORK ###
### LAYERS, MODEL & CALLBACK DEFINITIONS    ###
### by: OAMEED NOAKOASTEEN                  ###
###############################################

import os
import numpy                  as  np
import tensorflow             as  tf
from matplotlib import pyplot as plt

##############
### LAYERS ###
##############

def layers_ResBlock(X,FILTERS,KERNELSIZE,NAME):
 initializer=tf.keras.initializers.he_normal   ()
 if X.shape[-1]==FILTERS:
  STRIDES   =[1,1]
  short     =X
 else:
  STRIDES   =[2,2]
  short     =tf.keras.layers.Conv2D            (FILTERS                                 ,
                                                [1,1]                                   ,
                                                strides           =STRIDES              ,
                                                padding           ='same'               ,
                                                activation        =None                 ,
                                                kernel_initializer=initializer          ,
                                                name              =NAME+         '_'+'0' )(X    )
  short     =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn'+'_'+'0' )(short)
 x          =tf.keras.layers.Conv2D            (FILTERS                                 ,
                                                KERNELSIZE                              ,
                                                strides           =STRIDES              ,
                                                padding           ='same'               ,
                                                activation        =None                 ,
                                                kernel_initializer=initializer          ,
                                                name              =NAME+         '_'+'1' )(X)                                                
 x          =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn'+'_'+'1' )(x)
 x          =tf.keras.layers.ReLU              ()                                         (x)
 x          =tf.keras.layers.Conv2D            (FILTERS                                 ,
                                                KERNELSIZE                              ,
                                                strides           =[1,1]                ,
                                                padding           ='same'               ,
                                                activation        =None                 ,
                                                kernel_initializer=initializer          ,
                                                name              =NAME+         '_'+'2' )(x)                                               
 x          =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn'+'_'+'2' )(x)                                                
 x          =tf.keras.layers.Add               ()                                         ([x,short]) 
 x          =tf.keras.layers.ReLU              ()                                         (x)
 return x

def layers_conv2DTrans(X,FILTERS,KERNELSIZE,STRIDES,PADDING,NAME):
 initializer=tf.keras.initializers.he_normal   ()
 x          =tf.keras.layers.Conv2DTranspose   (FILTERS                        ,
                                                KERNELSIZE                     ,
                                                strides           =STRIDES     ,
                                                padding           =PADDING     ,
                                                activation        =None        ,
                                                kernel_initializer=initializer ,
                                                name              =NAME         )(X)
 x          =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn')(x)
 x          =tf.keras.layers.ReLU              ()                                (x)
 return x        

def layers_conv2D(X,FILTERS,KERNELSIZE,STRIDES,PADDING,NAME,BATCHNORM=True,LINEAR=False):
 initializer=tf.keras.initializers.he_normal   ()
 x          =tf.keras.layers.Conv2D            (FILTERS                        ,  
                                                KERNELSIZE                     ,
                                                strides           =STRIDES     ,
                                                padding           =PADDING     , 
                                                activation        =None        ,
                                                kernel_initializer=initializer , 
                                                name              =NAME         )(X)
 if     BATCHNORM:
  x         =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn')(x)
 if not LINEAR   :
  x         =tf.keras.layers.ReLU              ()                                (x)
 return x

#############
### MODEL ###
#############

def get_encoder(SHAPE):
 X    =tf.keras.Input (shape=SHAPE,            name='encoder_input')
 x    =layers_ResBlock(X,8 ,[3,3] ,                 'resblock_1'   )
 x    =layers_ResBlock(x,16,[3,3] ,                 'resblock_2'   )
 x    =layers_ResBlock(x,32,[3,3] ,                 'resblock_3'   )
 model=tf.keras.Model (inputs=X   , outputs=x, name='encoder'      )
 return model
 
class VanillaRNNCell(tf.keras.layers.Layer):
    
    def __init__(self, state_size):
        super().__init__()
        self.state_size=state_size
    
    def build(self, input_shape):
        self.Wx=self.add_weight(shape      =(input_shape[-1],self.state_size),
                                initializer='uniform'                        ,
                                name       ='Wx'                              )
        self.Wh=self.add_weight(shape      =(self.state_size,self.state_size),
                                initializer='uniform'                        ,
                                name       ='Wh'                              )                       
        self.bh=self.add_weight(shape      =(self.state_size,)               ,
                                initializer='uniform'                        ,
                                name       ='bh'                              )                                
        self.Wo=self.add_weight(shape      =(self.state_size,input_shape[-1]),
                                initializer='uniform'                        ,
                                name       ='Wo'                              )
        self.bo=self.add_weight(shape      =(input_shape[-1],)               ,
                                initializer='uniform'                        ,
                                name       ='bo'                              )
    
    def call(self,X,H):
        hidden=tf.linalg.matmul(X     ,self.Wx)+tf.linalg.matmul(H[0],self.Wh)+self.bh
        hidden=tf.math.tanh(hidden)
        output=tf.linalg.matmul(hidden,self.Wo)                               +self.bo
        return output, [hidden]
        
    def get_config(self):
        return {"state_size":self.state_size}

def get_decoder(SHAPE,CH):
 X    =tf.keras.Input(shape=SHAPE                , name='decoder_input'            )
 x    =layers_conv2DTrans(X,16,[3,3],[2,2],'same',      'conv_trans_1'             )
 x    =layers_conv2DTrans(x,8 ,[3,3],[2,2],'same',      'conv_trans_2'             )
 x    =layers_conv2DTrans(x,1 ,[3,3],[2,2],'same',      'conv_trans_3'             )
 x    =layers_conv2D     (x,CH,[3,3],[1,1],'same',      'conv_1'      , False, True)
 model=tf.keras.Model    (inputs=X, outputs=x    , name='decoder'                  )
 return model

def loss(TRUE,PRED):
 def loss_Frob(TRUE,PRED):
  x=tf.subtract   (TRUE, PRED          )
  x=tf.square     (x                   )
  x=tf.reduce_sum (x   , axis=[1,2,3,4])
  x=tf.multiply   (x   , 0.5           )
  x=tf.sqrt       (x                   )
  x=tf.reduce_mean(x                   )
  return x 
 def loss_GDL(YTRUE,YPRED):
  def GRAD(X):
   x=tf.square(X)
   x=tf.reduce_sum(x,axis=[1,2,3,4])
   x=tf.multiply(x,0.5)
   x=tf.reduce_mean(x)
   return x
  shape                =YTRUE.get_shape().as_list() 
  YTRUE_x_shifted_right=tf.slice(YTRUE,[0,0,1,0,0],[shape[0],shape[1],shape[2]-1,shape[3]  ,shape[4]])
  YTRUE_x_shifted_left =tf.slice(YTRUE,[0,0,0,0,0],[shape[0],shape[1],shape[2]-1,shape[3]  ,shape[4]])
  YTRUE_x_GRAD         =tf.abs  (YTRUE_x_shifted_right-YTRUE_x_shifted_left                          )
  YPRED_x_shifted_right=tf.slice(YPRED,[0,0,1,0,0],[shape[0],shape[1],shape[2]-1,shape[3]  ,shape[4]])
  YPRED_x_shifted_left =tf.slice(YPRED,[0,0,0,0,0],[shape[0],shape[1],shape[2]-1,shape[3]  ,shape[4]])
  YPRED_x_GRAD         =tf.abs  (YPRED_x_shifted_right-YPRED_x_shifted_left                          )
  LOSS_x_GRAD          =GRAD    (YPRED_x_GRAD-YTRUE_x_GRAD                                           )
  YTURE_y_shifted_right=tf.slice(YTRUE,[0,0,0,1,0],[shape[0],shape[1],shape[2]  ,shape[3]-1,shape[4]])
  YTURE_y_shifted_left =tf.slice(YTRUE,[0,0,0,0,0],[shape[0],shape[1],shape[2]  ,shape[3]-1,shape[4]])
  YTRUE_y_GRAD         =tf.abs  (YTURE_y_shifted_right-YTURE_y_shifted_left                          )
  YPRED_y_shifted_right=tf.slice(YPRED,[0,0,0,1,0],[shape[0],shape[1],shape[2]  ,shape[3]-1,shape[4]])
  YPRED_y_shifted_left =tf.slice(YPRED,[0,0,0,0,0],[shape[0],shape[1],shape[2]  ,shape[3]-1,shape[4]])
  YPRED_y_GRAD         =tf.abs  (YPRED_y_shifted_right-YPRED_y_shifted_left                          )
  LOSS_y_GRAD          =GRAD    (YPRED_y_GRAD-YTRUE_y_GRAD                                           )
  LOSS_GDL             =LOSS_x_GRAD+LOSS_y_GRAD
  return LOSS_GDL
 loss=loss_Frob(TRUE,PRED)+4e-3*loss_GDL(TRUE,PRED)
 return loss

class ERD(tf.keras.Model):

    def __init__(self, encoder, rnn, decoder, params, paths):
        super().__init__()
        self.encoder   =encoder
        self.rnn       =rnn
        self.decoder   =decoder
        self.params    =params
        self.paths     =paths
        self.samples   =[]
        self.samples_gt=[]
    
    def compile(self, optimizer, loss):
        super().compile()
        self.optimizer=optimizer
        self.loss     =loss

    def call(self, X, training=None):
        SHAPE=X.shape.as_list()
        x    =tf.transpose(X           , perm    =[1,0,2,3,4]                              )[0]
        x    =self.encoder(x           , training=True                                     )
        shape=x.shape.as_list()
        x    =tf.reshape  (x           , [-1, 1, np.prod(shape[1:])]                       )
        x    =tf.pad      (x           , paddings=tf.constant([[0,0],[0,SHAPE[1]-1],[0,0]]))
        x    =self.rnn    (x           , training=True                                     )
        x    =tf.reshape  (x           , [-1, SHAPE[1], shape[1], shape[2], shape[3]]      )
        x    =tf.transpose(x           , perm    =[1,0,2,3,4]                              )
        x    =tf.map_fn   (self.decoder, x                                                 )
        x    =tf.transpose(x           , perm    =[1,0,2,3,4]                              )
        return x

    def train_step(self, data):
        videos, _          =data
        with tf.GradientTape() as tape:
         predictions       =self         (videos,training=True)
         loss              =self.loss    (videos,predictions  )
        trainable_variables=self.encoder.trainable_weights+self.rnn.trainable_weights+self.decoder.trainable_weights
        grads              =tape.gradient(    loss , trainable_variables)
        self.optimizer.apply_gradients   (zip(grads, trainable_variables))
        return {"loss": loss}

##########################
### TRAINING CALLBACKS ###
##########################

class callback_custom_ckpt(tf.keras.callbacks.Callback):

    def on_epoch_end(self, epoch, logs=None):
        tf.keras.models.save_model(self.model ,self.model.paths[3])

class callback_custom_monitor(tf.keras.callbacks.Callback):

    def __init__(self, writer, data, plotter, converter):
        super().__init__()
        self.writer   =writer
        self.data     =data
        self.plotter  =plotter
        self.converter=converter

    def on_epoch_begin(self, epoch, logs=None):
        if epoch==0:
         self.model.samples_gt.append(self.data[0])
        x     =self.model(self.data)
        self.model.samples.append(x[0].numpy())
        figure=self.plotter('video', [x.numpy(),self.data], [])
        image =self.converter(figure)
        with self.writer.as_default():
         tf.summary.image("ERD Generated Frames (Color: Ground Truth)", image, step=epoch)

class callback_custom_history(tf.keras.callbacks.Callback):
    
    def __init__(self, plotter, whdf):
        super().__init__()
        self.plotter=plotter
        self.whdf   =whdf
    
    def on_train_begin(self, logs=None):
        self.metric_key=[]
        self.metric_one=[]
        self.metric_two=[]
   
    def on_train_batch_end(self, batch, logs=None):     
        keys=list(logs.keys())
        self.metric_one.append (logs[keys[0]])        
        if batch==0:
         self.metric_key.append(     keys[0] )
    
    def on_train_end(self, logs=None):
        savefilename=os.path.join(self.model.paths[4],'train','training')
        self.whdf(savefilename+'.h5'                                      ,
                  ['samples'                   , 'true'                  ],
                  [np.array(self.model.samples), self.model.samples_gt[0]] )
        savefilename=os.path.join(self.model.paths[4],'train','metrics' )
        self.whdf(savefilename+'.h5'  ,
                  [self.metric_key[0]],
                  [self.metric_one   ] )
        fig=self.plotter('metrics'             ,
                         [[self.metric_key[0]],
                          [self.metric_one   ]],
                         [['b']               ] )
        plt.savefig(savefilename+'.png',format='png')


