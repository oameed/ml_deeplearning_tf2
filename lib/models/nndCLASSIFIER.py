############################################
### DEEP LEARNING WITH TENSORFLOW 2      ###
### TRAINING CLASSIFIER                  ###
### LAYERS, MODEL & CALLBACK DEFINITIONS ###
### by: OAMEED NOAKOASTEEN               ###
############################################

import os
import numpy                  as  np
import tensorflow             as  tf
from matplotlib import pyplot as plt

##############
### LAYERS ###
##############
def layers_conv2D(X,FILTERS,KERNELSIZE,STRIDES,PADDING,NAME,BATCHNORM=True,LINEAR=False):
 initializer=tf.keras.initializers.he_normal   ()
 x          =tf.keras.layers.Conv2D            (FILTERS                        ,  
                                                KERNELSIZE                     ,
                                                strides           =STRIDES     ,
                                                padding           =PADDING     , 
                                                activation        =None        ,
                                                kernel_initializer=initializer , 
                                                name              =NAME         )(X)
 if     BATCHNORM:
  x         =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn')(x)
 if not LINEAR   :
  x         =tf.keras.layers.ReLU              ()                                (x)
 return x

def layers_fc(X,UNITS,NAME,BATCHNORM=True,LINEAR=False):
 initializer=tf.keras.initializers.he_normal   ()
 x          =tf.keras.layers.Dense             (UNITS                          ,
                                                activation        =None        ,
                                                kernel_initializer=initializer ,
                                                name              =NAME         )(X)
 if     BATCHNORM:
  x         =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn')(x)
 if not LINEAR   :
  x         =tf.keras.layers.ReLU              ()                                (x)
 return x

#############
### MODEL ###
#############

def get_encoder(SHAPE):
 X    =tf.keras.Input(shape=SHAPE             , name='input'             )
 x    =layers_conv2D (X,128,[3,3],[2,2],'same',      'conv_1'            )
 x    =layers_conv2D (x,128,[3,3],[2,2],'same',      'conv_2'            )
 x    =tf.keras.layers.Flatten()(x)
 x    =layers_fc     (x,10 ,                         'flc_1', False, True)
 model=tf.keras.Model(inputs=X   , outputs=x  , name='encoder'           )
 return model

class classifier(tf.keras.Model):

    def __init__(self, network, paths, params):
        super().__init__()
        self.network=network
        self.paths  =paths
        self.params =params

    def call(self, X, training=None):
        x=self.network(X, training=training)
        return x
    
    def train_step(self, data):
     img,lab=data
     with tf.GradientTape() as tape:
      predictions=self(img, training=True)
      loss       =self.compiled_loss(lab, predictions, regularization_losses=self.losses)
     gradients   =tape.gradient        (    loss     , self.network.trainable_variables )
     self.optimizer.apply_gradients    (zip(gradients, self.network.trainable_variables))
     self.compiled_metrics.update_state(    lab      , predictions                      )
     return {"loss"    :loss                                           , 
             "accuracy":self.compiled_metrics._user_metrics[0].result() }

##########################
### TRAINING CALLBACKS ###
##########################

class callback_custom_ckpt(tf.keras.callbacks.Callback):

    def on_epoch_end(self, epoch, logs=None):
        tf.keras.models.save_model(self.model ,self.model.paths[3])


class callback_custom_monitor(tf.keras.callbacks.Callback):

    def __init__(self, writer, data, plotter, converter):
        super().__init__()
        self.writer   =writer
        self.data     =data
        self.plotter  =plotter
        self.converter=converter

    def on_epoch_begin(self, epoch, logs=None):
        img,lab=self.data
        x      =self.model(img)
        x      =tf.keras.layers.Softmax()(x)
        x      =np.array([np.argmax(i) for i in x])
        figure=self.plotter  ('cm', [lab,x], [self.model.params[4]])
        image =self.converter(figure)
        with self.writer.as_default():
         tf.summary.image("Confusion Matrix", image, step=epoch)


class callback_custom_history(tf.keras.callbacks.Callback):
    
    def __init__(self, plotter, whdf):
        super().__init__()
        self.plotter=plotter
        self.whdf   =whdf
    
    def on_train_begin(self, logs=None):
        self.metric_key=[]
        self.metric_one=[]
        self.metric_two=[]
   
    def on_train_batch_end(self, batch, logs=None):     
        keys=list(logs.keys())
        self.metric_one.append (logs[keys[0]])
        self.metric_two.append (logs[keys[1]])
        if batch==0:
         self.metric_key.append(     keys[0] )
         self.metric_key.append(     keys[1] )
    
    def on_train_end(self, logs=None):
        savefilename=os.path.join(self.model.paths[4],'train','metrics')
        self.whdf(savefilename+'.h5'                      ,
                  [self.metric_key[0], self.metric_key[1]],
                  [self.metric_one   , self.metric_two   ] )
        fig=self.plotter('metrics'              ,
                         [[self.metric_key[0]],
                          [self.metric_one   ] ],
                         [['b'               ] ] )
        plt.savefig(savefilename+'.png',format='png')


