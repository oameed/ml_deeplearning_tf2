############################################
### DEEP LEARNING WITH TENSORFLOW 2      ###
### TRAINING AUTOENCODER                 ###
### LAYERS, MODEL & CALLBACK DEFINITIONS ###
### by: OAMEED NOAKOASTEEN               ###
############################################

import os
import numpy                  as  np
import tensorflow             as  tf
from matplotlib import pyplot as plt

##############
### LAYERS ###
##############
def layers_conv2D(X,FILTERS,KERNELSIZE,STRIDES,PADDING,NAME,BATCHNORM=True,LINEAR=False):
 initializer=tf.keras.initializers.he_normal   ()
 x          =tf.keras.layers.Conv2D            (FILTERS                        ,  
                                                KERNELSIZE                     ,
                                                strides           =STRIDES     ,
                                                padding           =PADDING     , 
                                                activation        =None        ,
                                                kernel_initializer=initializer , 
                                                name              =NAME         )(X)
 if     BATCHNORM:
  x         =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn')(x)
 if not LINEAR   :
  x         =tf.keras.layers.ReLU              ()                                (x)
 return x

def layers_conv2DTrans(X,FILTERS,KERNELSIZE,STRIDES,PADDING,NAME,BATCHNORM=True,LINEAR=False):
 initializer=tf.keras.initializers.he_normal   ()
 x          =tf.keras.layers.Conv2DTranspose   (FILTERS                        ,
                                                KERNELSIZE                     ,
                                                strides           =STRIDES     ,
                                                padding           =PADDING     ,
                                                activation        =None        ,
                                                kernel_initializer=initializer ,
                                                name              =NAME         )(X)
 if     BATCHNORM:
  x         =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn')(x)
 if not LINEAR   :
  x         =tf.keras.layers.ReLU              ()                                (x)
 return x

#############
### MODEL ###
#############
 
def get_encoder_decoder(SHAPE):
 X    =tf.keras.Input    (shape =SHAPE                    , name='input'                    )
 x    =layers_conv2D     (X,64         ,[3,3],[2,2],'same',      'conv_1'                   )
 x    =layers_conv2D     (x,128        ,[3,3],[2,2],'same',      'conv_2'                   )
 x    =layers_conv2DTrans(x,64         ,[3,3],[2,2],'same',      'conv_trans_1'             )
 x    =layers_conv2DTrans(x,X.shape[-1],[3,3],[2,2],'same',      'conv_trans_2', False, True)
 model=tf.keras.Model    (inputs=X     , outputs=x        , name='network'                  )
 return model

class AE(tf.keras.Model):

    def __init__(self, network, paths, params):
        super().__init__()
        self.network=network
        self.paths  =paths
        self.params =params
   
    def compile(self, optimizer, loss):
        super().compile()
        self.optimizer=optimizer
        self.loss     =loss

    def call(self, X, training=None):
        x=self.network(X, training=training)
        return x

    def train_step(self, data):
        img,lab=data
        with tf.GradientTape() as tape:
         predictions       =self(img,training=True)
         loss              =self.loss(img,predictions)
        gradients          =tape.gradient(    loss     , self.network.trainable_variables )
        self.optimizer.apply_gradients   (zip(gradients, self.network.trainable_variables))
        return {'loss': loss}        

##########################
### TRAINING CALLBACKS ###
##########################

class callback_custom_ckpt(tf.keras.callbacks.Callback):

    def on_epoch_end(self, epoch, logs=None):
        tf.keras.models.save_model(self.model ,self.model.paths[3])

class callback_custom_monitor(tf.keras.callbacks.Callback):

    def __init__(self, writer, data, plotter, converter):
        super().__init__()
        self.writer   =writer
        self.data     =data
        self.plotter  =plotter
        self.converter=converter

    def on_epoch_begin(self, epoch, logs=None):        
        img,lab=self.data
        idx    =[i for i in range(img.shape[0])]
        idx    =np.random.choice(idx, size=100, replace=False)
        img    =img[idx]
        x      =self.model    (img                   )
        figure =self.plotter  ('collage', x, ['gray'])
        image  =self.converter(figure)
        with self.writer.as_default():
         tf.summary.image("Reconstructed Images", image, step=epoch)

class callback_custom_history(tf.keras.callbacks.Callback):
    
    def __init__(self, plotter, whdf):
        super().__init__()
        self.plotter=plotter
        self.whdf   =whdf
    
    def on_train_begin(self, logs=None):
        self.metric_key=[]
        self.metric_one=[]
   
    def on_train_batch_end(self, batch, logs=None):     
        keys=list(logs.keys())
        self.metric_one.append (logs[keys[0]])        
        if batch==0:
         self.metric_key.append(     keys[0] )
    
    def on_train_end(self, logs=None):
        savefilename=os.path.join(self.model.paths[4],'train','metrics')
        self.whdf(savefilename+'.h5'  ,
                  [self.metric_key[0]],
                  [self.metric_one   ] )
        fig=self.plotter('metrics'              ,
                         [[self.metric_key[0]],
                          [self.metric_one   ] ],
                         [['b'               ] ] )
        plt.savefig(savefilename+'.png',format='png')


