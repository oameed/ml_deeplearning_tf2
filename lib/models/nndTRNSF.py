######################################################
### DEEP LEARNING WITH TENSORFLOW 2                ###
### TRAINING TRANSFORMER                           ### 
### LAYERS, MODEL & CALLBACK DEFINITIONS           ###
### by: OAMEED NOAKOASTEEN                         ###
######################################################

import os
import numpy                  as  np
import tensorflow             as  tf
from matplotlib import pyplot as plt

def positional_encoding(length, depth):
 depth       = depth/2
 positions   = np.arange(length)[:, np.newaxis]     
 depths      = np.arange(depth)[np.newaxis, :]/depth
 angle_rates = 1 / (10000**depths)         
 angle_rads  = positions * angle_rates     
 pos_encoding= np.concatenate([np.sin(angle_rads), np.cos(angle_rads)],axis=-1)
 return tf.cast(pos_encoding, dtype=tf.float32)

##############
### LAYERS ###
##############
class PositionalEmbedding(tf.keras.layers.Layer):
    
    def __init__(self               ,
                 vocab_size         ,
                 d_model            ,
                 POSITIONAL_ENCODING ):
     super().__init__()
     self.d_model     = d_model
     self.embedding   = tf.keras.layers.Embedding(vocab_size, d_model, mask_zero=True) 
     self.pos_encoding= POSITIONAL_ENCODING(length=2048, depth=d_model)               

    #def compute_mask(self, *args, **kwargs):
    # return self.embedding.compute_mask(*args, **kwargs)

    def call(self, x):
     length = tf.shape(x)[1]
     x      = self.embedding(x)
     x     *= tf.math.sqrt(tf.cast(self.d_model, tf.float32))
     x      = x + self.pos_encoding[tf.newaxis, :length, :]
     return x

class BaseAttention(tf.keras.layers.Layer):
    
    def __init__(self, **kwargs):
     super().__init__()
     self.mha             = tf.keras.layers.MultiHeadAttention(**kwargs)
     self.layernorm       = tf.keras.layers.LayerNormalization()
     self.add             = tf.keras.layers.Add()

class CrossAttention(BaseAttention):
    
    def call(self, x, context):
     attn_output, attn_scores = self.mha(query=x, key=context, value=context, return_attention_scores=True)
     x                        = self.add([x, attn_output])
     x                        = self.layernorm(x)
     return x, attn_scores

class GlobalSelfAttention(BaseAttention):
    
    def call(self, x):
     attn_output = self.mha(query=x, value=x, key=x)
     x           = self.add([x, attn_output])
     x           = self.layernorm(x)
     return x

class CausalSelfAttention(BaseAttention):
    
    def call(self, x):
     attn_output = self.mha(query=x, value=x, key=x, use_causal_mask = True)
     x           = self.add([x, attn_output])
     x           = self.layernorm(x)
     return x

class FeedForward(tf.keras.layers.Layer):
    
    def __init__(self            ,
                 d_model         ,
                 dff             ,
                 dropout_rate=0.1 ):
     super().__init__()
     self.seq        = tf.keras.Sequential([tf.keras.layers.Dense  (dff         , activation='relu'),
                                            tf.keras.layers.Dense  (d_model                        ),
                                            tf.keras.layers.Dropout(dropout_rate                   ) ])
     self.add        = tf.keras.layers.Add()
     self.layer_norm = tf.keras.layers.LayerNormalization()

    def call(self, x):
     x = self.add       ([x, self.seq(x)])
     x = self.layer_norm(x) 
     return x

class EncoderLayer(tf.keras.layers.Layer):
    
    def __init__(self               ,
                 *                  ,
                 d_model            ,
                 num_heads          ,
                 dff                ,
                 GLOBALSELFATTENTION,
                 FEEDFORWARD        ,
                 dropout_rate=0.1    ):
     super().__init__()
     self.self_attention = GLOBALSELFATTENTION(num_heads=num_heads, key_dim=d_model, dropout=dropout_rate) 
     self.ffn            = FEEDFORWARD        (d_model, dff)                                               
    
    def call(self, x):
     x = self.self_attention(x)
     x = self.ffn           (x)
     return x

class DecoderLayer(tf.keras.layers.Layer):
    
    def __init__(self               ,
                 *                  ,
                 d_model            ,
                 num_heads          ,
                 dff                ,
                 CAUSALSELFATTENTION,
                 CROSSATTENTION     ,
                 FEEDFORWARD        ,
                 dropout_rate=0.1    ):
     super().__init__()
     self.causal_self_attention = CAUSALSELFATTENTION(num_heads=num_heads, key_dim=d_model, dropout=dropout_rate) 
     self.cross_attention       = CROSSATTENTION     (num_heads=num_heads, key_dim=d_model, dropout=dropout_rate) 
     self.ffn                   = FEEDFORWARD        (d_model, dff)

    def call(self, x, context):
     x                     = self.causal_self_attention(x=x)
     x, attn_scores        = self.cross_attention      (x=x, context=context)
     x                     = self.ffn                  (x)
     return x, attn_scores

#############
### MODEL ###
#############
class Encoder(tf.keras.layers.Layer):
    
    def __init__(self               ,
                 *                  ,
                 num_layers         ,
                 d_model            ,
                 num_heads          ,
                 dff                , 
                 vocab_size         ,
                 POSITIONAL_ENCODING,
                 POSITIONALEMBEDDING,
                 GLOBALSELFATTENTION,
                 FEEDFORWARD        ,
                 ENCODERLAYER       ,
                 dropout_rate=0.1    ):
     super().__init__()
     self.d_model      = d_model
     self.num_layers   = num_layers
     self.pos_embedding= POSITIONALEMBEDDING(vocab_size         =vocab_size         ,
                                             d_model            =d_model            ,
                                             POSITIONAL_ENCODING=POSITIONAL_ENCODING )                      
     self.enc_layers   = [ENCODERLAYER(d_model            =d_model            , 
                                       num_heads          =num_heads          , 
                                       dff                =dff                ,
                                       GLOBALSELFATTENTION=GLOBALSELFATTENTION,
                                       FEEDFORWARD        =FEEDFORWARD        ,
                                       dropout_rate       =dropout_rate        ) for _ in range(num_layers)] 
     self.dropout      = tf.keras.layers.Dropout(dropout_rate)

    def call(self, x):
     x  = self.pos_embedding(x)
     x  = self.dropout(x)
     for i in range(self.num_layers):
      x = self.enc_layers[i](x)
     return x                    

class Decoder(tf.keras.layers.Layer):
    
    def __init__(self               ,
                 *                  ,
                 num_layers         ,
                 d_model            ,
                 num_heads          ,
                 dff                ,
                 vocab_size         ,
                 POSITIONAL_ENCODING,
                 POSITIONALEMBEDDING,
                 CAUSALSELFATTENTION,
                 CROSSATTENTION     ,
                 FEEDFORWARD        ,
                 DECODERLAYER       ,
                 dropout_rate=0.1    ):
     super().__init__()
     self.d_model          = d_model
     self.num_layers       = num_layers
     self.pos_embedding    = POSITIONALEMBEDDING(vocab_size         =vocab_size         ,
                                                 d_model            =d_model            ,
                                                 POSITIONAL_ENCODING=POSITIONAL_ENCODING )
     self.dropout          = tf.keras.layers.Dropout(dropout_rate)
     self.dec_layers       = [DECODERLAYER(d_model            =d_model            , 
                                           num_heads          =num_heads          , 
                                           dff                =dff                , 
                                           CAUSALSELFATTENTION=CAUSALSELFATTENTION,
                                           CROSSATTENTION     =CROSSATTENTION     ,
                                           FEEDFORWARD        =FEEDFORWARD        ,
                                           dropout_rate       =dropout_rate        ) for _ in range(num_layers)]
    
    def call(self, x, context):
     x = self.pos_embedding(x)
     x = self.dropout      (x)
     for i in range(self.num_layers):
      x, attn_scores  = self.dec_layers[i](x, context)
     return x, attn_scores

class Transformer(tf.keras.Model):
    
    def __init__(self               ,
                 *                  ,
                 num_layers         ,  
                 d_model            ,
                 num_heads          ,
                 dff                , 
                 input_vocab_size   ,  
                 target_vocab_size  ,
                 POSITIONAL_ENCODING,
                 POSITIONALEMBEDDING,
                 CROSSATTENTION     ,
                 GLOBALSELFATTENTION,
                 CAUSALSELFATTENTION,
                 FEEDFORWARD        , 
                 ENCODERLAYER       ,
                 DECODERLAYER       ,
                 ENCODER            ,
                 DECODER            ,
                 paths              ,
                 dropout_rate=0.1    ):
     super().__init__()
     self.encoder     = ENCODER(num_layers         =num_layers         ,  
                                d_model            =d_model            ,
                                num_heads          =num_heads          ,  
                                dff                =dff                ,
                                vocab_size         =input_vocab_size   ,
                                POSITIONAL_ENCODING=POSITIONAL_ENCODING,
                                POSITIONALEMBEDDING=POSITIONALEMBEDDING,
                                GLOBALSELFATTENTION=GLOBALSELFATTENTION,
                                FEEDFORWARD        =FEEDFORWARD        ,
                                ENCODERLAYER       =ENCODERLAYER       ,
                                dropout_rate       =dropout_rate        ) 

     self.decoder     = DECODER(num_layers         =num_layers         ,   
                                d_model            =d_model            ,
                                num_heads          =num_heads          ,  
                                dff                =dff                ,
                                vocab_size         =target_vocab_size  ,
                                POSITIONAL_ENCODING=POSITIONAL_ENCODING,
                                POSITIONALEMBEDDING=POSITIONALEMBEDDING,
                                CAUSALSELFATTENTION=CAUSALSELFATTENTION,
                                CROSSATTENTION     =CROSSATTENTION     ,
                                FEEDFORWARD        =FEEDFORWARD        ,
                                DECODERLAYER       =DECODERLAYER       ,
                                dropout_rate       =dropout_rate        ) 

     self.final_layer = tf.keras.layers.Dense(target_vocab_size)
     self.paths       = paths
    
    def compile(self, optimizer, loss, accuracy):
     super().compile()
     self.optimizer=optimizer
     self.loss     =loss
     self.accuracy =accuracy

    def call(self, inputs):
     context, x     = inputs
     context        = self.encoder    (   context)
     x, attn_scores = self.decoder    (x, context)
     logits         = self.final_layer(x) 
     #try: # Drop the keras mask, so it doesn't scale the losses/metrics /// b/250038731
     # del logits._keras_mask
     #except AttributeError:
     # pass
     return logits, attn_scores

    def train_step(self, data):
     inputs, label = data
     with tf.GradientTape() as tape:
      predictions, _ =self           (inputs               )
      loss           =self.loss      (label ,predictions   )
      accuracy       =self.accuracy  (label ,predictions   )
     gradients=tape.gradient       (    loss      , self.trainable_weights)
     self.optimizer.apply_gradients(zip(gradients , self.trainable_weights))
     return {'loss':loss, 'accuracy':accuracy}

class CustomSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
    
    def __init__(self, d_model, warmup_steps=4000):
     super().__init__()
     self.d_model      = d_model
     self.d_model      = tf.cast(self.d_model, tf.float32)
     self.warmup_steps = warmup_steps
    
    def __call__(self, step):
     step = tf.cast      (step, dtype=tf.float32)
     arg1 = tf.math.rsqrt(step)
     arg2 = step * (self.warmup_steps ** -1.5)
     return tf.math.rsqrt(self.d_model) * tf.math.minimum(arg1, arg2)

    def get_config(self):
     return {"d_model":self.d_model.numpy(),"warmup_steps":self.warmup_steps}

def masked_loss(label, pred):
 mask        = label != 0
 loss_object = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True, reduction='none')
 loss        = loss_object(label, pred)
 mask        = tf.cast(mask, dtype=loss.dtype)
 loss       *= mask
 loss        = tf.reduce_sum(loss)/tf.reduce_sum(mask)
 return loss

def masked_accuracy(label, pred):
 pred  = tf.argmax(pred , axis=2    )
 label = tf.cast  (label, pred.dtype)
 match = label == pred
 mask  = label != 0
 match = match & mask
 match = tf.cast  (match, dtype=tf.float32)
 mask  = tf.cast  (mask , dtype=tf.float32)
 return tf.reduce_sum(match)/tf.reduce_sum(mask)

class Translator(tf.Module):
    
    def __init__(self, tokenizers, transformer):
     self.tokenizers = tokenizers
     self.transformer= transformer

    def __call__(self, sentence, max_length=128):
     assert isinstance(sentence, tf.Tensor)
     if len(sentence.shape) == 0:
      sentence       = sentence[tf.newaxis]
     sentence        = self.tokenizers.pt.tokenize(sentence).to_tensor()         # The input sentence is Portuguese, hence adding the `[START]` and `[END]` tokens.
     encoder_input   = sentence
     start_end       = self.tokenizers.en.tokenize([''])[0]                      # As the output language is English, initialize the output with the English `[START]` token.
     start           = start_end[0][tf.newaxis]
     end             = start_end[1][tf.newaxis]
     output_array    = tf.TensorArray(dtype=tf.int64, size=0, dynamic_size=True) # `tf.TensorArray` (instead of a Python list), so that the dynamic-loop can be traced by `tf.function`.
     output_array    = output_array.write(0, start)
     for i in tf.range(max_length):
      output         = tf.transpose(output_array.stack())
      predictions, _ = self.transformer((encoder_input, output), training=False)
      predictions    = predictions[:, -1:, :]                                    # Select the last token from the `seq_len` dimension. Shape `(batch_size, 1, vocab_size)`.
      predicted_id   = tf.argmax(predictions, axis=-1)
      output_array   = output_array.write(i+1, predicted_id[0])                  # Concatenate the `predicted_id` to the output which is given to the decoder as its input.
      if predicted_id== end:
        break
     output          = tf.transpose(output_array.stack())                        # The output shape is `(1, tokens)`.    
     text            = self.tokenizers.en.detokenize(output)[0]                  # Shape: `()`.
     tokens          = self.tokenizers.en.lookup(output)[0]     
     _, attn_weights = self.transformer((encoder_input, output[:,:-1]), training=False)
     return text, tokens, attn_weights

##########################
### TRAINING CALLBACKS ###
##########################
class callback_custom_ckpt(tf.keras.callbacks.Callback):

    def on_epoch_end(self, epoch, logs=None):
        tf.keras.models.save_model(self.model ,self.model.paths[3])

class callback_custom_history(tf.keras.callbacks.Callback):
    
    def __init__(self, plotter, whdf):
        super().__init__()
        self.plotter=plotter
        self.whdf   =whdf
    
    def on_train_begin(self, logs=None):
        self.metric_one  =[]
        self.metric_two  =[]

    def on_train_batch_end(self, batch, logs=None):
        keys=[i for i in logs.keys()]
        self.metric_one.append  (logs[keys[0]])
        self.metric_two.append  (logs[keys[1]])
    
    def on_train_end(self, logs=None):
        keys        =[i for i in logs.keys()]
        savefilename=os.path.join(self.model.paths[4],'train','metrics')
        self.whdf(savefilename+'.h5'                      ,
                  [keys[0]           , keys[1]],
                  [self.metric_one   , self.metric_two   ] )
        fig=self.plotter('metrics'              ,
                         [[keys           [0]],
                          [self.metric_one   ] ],
                         [['b'               ] ] )
        plt.savefig(savefilename+'_'+'loss'    +'.png',format='png')
        fig=self.plotter('metrics'              ,
                         [[keys           [1]],
                          [self.metric_two   ] ],
                         [['b'               ] ] )
        plt.savefig(savefilename+'_'+'accuracy'+'.png',format='png')


