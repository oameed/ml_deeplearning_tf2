######################################################
### DEEP LEARNING WITH TENSORFLOW 2                ###
### TRAINING GENERATIVE ADVERSESRIAL NETWORK (GAN) ### 
### LAYERS, MODEL & CALLBACK DEFINITIONS           ###
### by: OAMEED NOAKOASTEEN                         ###
######################################################

import os
import numpy                  as  np
import tensorflow             as  tf
from matplotlib import pyplot as plt

##############
### LAYERS ###
##############
def layers_fc(X,UNITS,NAME,BATCHNORM=True,LINEAR=False):
 initializer=tf.keras.initializers.he_normal   ()
 x          =tf.keras.layers.Dense             (UNITS                          ,
                                                activation        =None        ,
                                                kernel_initializer=initializer ,
                                                name              =NAME         )(X)
 if     BATCHNORM:
  x         =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn')(x)
 if not LINEAR   :
  x         =tf.keras.layers.ReLU              ()                                (x)
 return x

def layers_conv2D(X,FILTERS,KERNELSIZE,STRIDES,PADDING,NAME,BATCHNORM=True,LINEAR=False):
 initializer=tf.keras.initializers.he_normal   ()
 x          =tf.keras.layers.Conv2D            (FILTERS                        ,  
                                                KERNELSIZE                     ,
                                                strides           =STRIDES     ,
                                                padding           =PADDING     , 
                                                activation        =None        ,
                                                kernel_initializer=initializer , 
                                                name              =NAME         )(X)
 if     BATCHNORM:
  x         =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn')(x)
 if not LINEAR   :
  x         =tf.keras.layers.LeakyReLU         (alpha=0.2)                       (x)
 return x

def layers_conv2DTrans(X,FILTERS,KERNELSIZE,STRIDES,PADDING,NAME,BATCHNORM=True,LINEAR=False):
 initializer=tf.keras.initializers.he_normal   ()
 x          =tf.keras.layers.Conv2DTranspose   (FILTERS                        ,
                                                KERNELSIZE                     ,
                                                strides           =STRIDES     ,
                                                padding           =PADDING     ,
                                                activation        =None        ,
                                                kernel_initializer=initializer ,
                                                name              =NAME         )(X)
 if     BATCHNORM:
  x         =tf.keras.layers.BatchNormalization(name              =NAME+'_'+'bn')(x)
 if not LINEAR   :
  x         =tf.keras.layers.LeakyReLU         (alpha=0.2)                       (x)
 return x

#############
### MODEL ###
#############

def get_discriminator(SHAPE):
 X    =tf.keras.Input         (shape=SHAPE                        , name='input_disc'               )
 x    =layers_conv2D          (X,32           ,[3,3],[2,2],'same' ,      'conv_1'      , False      )
 x    =layers_conv2D          (x,64           ,[3,3],[2,2],'same' ,      'conv_2'      , False      )
 x    =tf.keras.layers.Flatten()(x)
 x    =layers_fc              (x,128          ,                          'fc_1'        , False      )
 x    =layers_fc              (x,1            ,                          'fc_2'        , False, True)
 model=tf.keras.Model         (inputs=X       , outputs=x         , name='discriminator'            )
 return model

def get_generator(SHAPE,SIZE,CH):
 X    =tf.keras.Input         (shape =SIZE                        , name='input_genr'               )
 x    =layers_fc              (X,np.prod(SHAPE)                   ,      'fc_3'                     )
 x    =tf.keras.layers.Reshape(SHAPE)(x)
 x    =layers_conv2DTrans     (x,64            ,[3,3],[2,2],'same',      'conv_trans_1'             )
 x    =layers_conv2DTrans     (x,32            ,[3,3],[2,2],'same',      'conv_trans_2'             )
 x    =layers_conv2D          (x,CH            ,[3,3],[1,1],'same',      'conv_3'      , True , True)
 x    =tf.keras.layers.Lambda (tf.keras.activations.tanh)(x)
 model=tf.keras.Model         (inputs=X        , outputs=x        , name='generator'                )
 return model

def z_sample(SHAPE):
 return tf.random.normal(SHAPE)

def loss_wasserstein_disc(REAL,FAKE,PENALTY,LAMBDA):
 loss_mean_real=tf.reduce_mean(tf.math.multiply(REAL,-tf.ones_like (REAL)))
 loss_mean_fake=tf.reduce_mean(tf.math.multiply(FAKE, tf.ones_like (FAKE)))
 loss          =loss_mean_real+loss_mean_fake+LAMBDA*PENALTY
 return loss, loss_mean_real, loss_mean_fake 

def loss_wasserstein_genr(     FAKE):
 loss_mean_fake=tf.reduce_mean(tf.math.multiply(FAKE,-tf.ones_like (FAKE)))
 return loss_mean_fake

class GAN(tf.keras.Model):

    def __init__(self, discriminator, generator, paths, params):
        super().__init__()
        self.discriminator  =discriminator
        self.generator      =generator
        self.paths          =paths
        self.params         =params
        self.samples        =[]

    def compile(self, d_optimizer, g_optimizer, loss_disc, loss_genr):
        super().compile()
        self.d_optimizer    =d_optimizer
        self.g_optimizer    =g_optimizer
        self.loss_disc      =loss_disc
        self.loss_genr      =loss_genr

    def call(self, X, training=None):
        x=self.generator(X, training=training)
        return x

    def train_step(self, data):
        
        LAMBDA              =10
        loss_disc_real_temp =[]
        loss_disc_fake_temp =[]
        
        img_real,lab        =data
        
        for _ in range(5):
         with tf.GradientTape() as tape:
          Z                 =z_sample          ([self.params[0][3], self.params[0][7]]               )
          img_fake          =self.generator    (Z                 , training=True                    )
          logit_real        =self.discriminator(img_real          , training=True                    )
          logit_fake        =self.discriminator(img_fake          , training=True                    )

          alpha             =tf.random.normal([self.params[0][3],1])                                    # BEGIN CALCULATING GRADIENT PENALTY
          img_real_flat     =tf.reshape(img_real,[-1,np.prod(img_real.shape[1:])])
          img_fake_flat     =tf.reshape(img_fake,[-1,np.prod(img_fake.shape[1:])])
          delta             =img_fake_flat-img_real_flat
          interpolates      =img_real_flat+(alpha*delta)
          interpolates_2D   =tf.reshape(interpolates,[-1]+self.params[2])
          logit_interpolates=self.discriminator(interpolates_2D   , training=True                  )
          grads_penalty     =tf.gradients(logit_interpolates, [interpolates]                       )[0]
          slopes            =tf.math.sqrt(tf.math.reduce_sum(tf.math.square(grads_penalty), axis=1))
          gradient_penalty  =tf.reduce_mean((slopes-1)**2)                                              # END  CALCULATING GRADIENT PENALTY

          loss_disc,lmr,lmf =self.loss_disc    (logit_real, logit_fake, gradient_penalty, LAMBDA     )
          loss_disc_real_temp.append(lmr)                                                               # DISCRIMINATOR'S LOSS: FROM REAL IMAGES
          loss_disc_fake_temp.append(lmf)                                                               # DISCRIMINATOR'S LOSS: FROM FAKE IMAGES
         grads_disc         =tape.gradient     (    loss_disc , self.discriminator.trainable_weights )
         self.d_optimizer.apply_gradients      (zip(grads_disc, self.discriminator.trainable_weights))
        
        with tf.GradientTape() as tape:
         Z                  =z_sample          ([self.params[0][3], self.params[0][7]])
         img_fake           =self.generator    (Z                 , training=True     )
         logit_fake         =self.discriminator(img_fake          , training=True     )
         loss_genr          =self.loss_genr    (            logit_fake                )
        grads_genr          =tape.gradient     (    loss_genr     , self.generator.trainable_weights )
        self.g_optimizer.apply_gradients       (zip(grads_genr    , self.generator.trainable_weights))
        return {"d_loss_real": tf.reduce_mean(loss_disc_real_temp),
                "d_loss_fake": tf.reduce_mean(loss_disc_fake_temp),
                "d_loss"     : loss_disc                          ,
                "g_loss"     : loss_genr                          ,
                "penalty"    : LAMBDA*gradient_penalty             }

##########################
### TRAINING CALLBACKS ###
##########################

class callback_custom_ckpt(tf.keras.callbacks.Callback):

    def on_epoch_end(self, epoch, logs=None):
        tf.keras.models.save_model(self.model ,self.model.paths[3])

class callback_custom_monitor(tf.keras.callbacks.Callback):

    def __init__(self, writer, data, plotter, converter):
        super().__init__()
        self.writer   =writer
        self.data     =data
        self.plotter  =plotter
        self.converter=converter
    
    def on_epoch_begin(self, epoch, logs=None):
        x     =self.model(self.data).numpy()
        self.model.samples.append(x)
        if  self.model.params[0][0] in ['type1','type2']:
         colormap ='gray'
        else:
         if self.model.params[0][0] in ['type3'        ]:
          colormap='viridis'
        figure=self.plotter('collage', x, [colormap]     )
        image =self.converter(figure)
        with self.writer.as_default():
         tf.summary.image("GAN Generated Images", image, step=epoch)

class callback_custom_history(tf.keras.callbacks.Callback):
    
    def __init__(self, plotter, whdf):
        super().__init__()
        self.plotter=plotter
        self.whdf   =whdf
    
    def on_train_begin(self, logs=None):
        self.metric_key  =[]
        self.metric_one  =[]
        self.metric_two  =[]
        self.metric_three=[]
        self.metric_four =[]
        self.metric_five =[]

    def on_train_batch_end(self, batch, logs=None):     
        keys=list(logs.keys())
        self.metric_one.append  (logs[keys[0]])
        self.metric_two.append  (logs[keys[1]])
        self.metric_three.append(logs[keys[2]])
        self.metric_four.append (logs[keys[3]])
        self.metric_five.append (logs[keys[4]])
        if batch==0:
         self.metric_key.append (     keys[0] )
         self.metric_key.append (     keys[1] )
         self.metric_key.append (     keys[2] )
         self.metric_key.append (     keys[3] )
         self.metric_key.append (     keys[4] )
    
    def on_train_end(self, logs=None):
        savefilename=os.path.join(self.model.paths[4],'train','training')
        self.whdf(savefilename+'.h5'            ,
                  ['samples'                   ],
                  [np.array(self.model.samples)] )
        savefilename=os.path.join(self.model.paths[4],'train','metrics')
        self.whdf(savefilename+'.h5'                               ,
                  [self.metric_key[0], self.metric_key[1], self.metric_key[2], self.metric_key[3], self.metric_key[4]],
                  [self.metric_one   , self.metric_two   , self.metric_three , self.metric_four  , self.metric_five  ] )
        fig=self.plotter('metrics'                                                                                              ,
                         [[self.metric_key[0], self.metric_key[1], self.metric_key[2], self.metric_key[3], self.metric_key[4]],
                          [self.metric_one   , self.metric_two   , self.metric_three , self.metric_four  , self.metric_five  ] ],
                         [['lightsteelblue'  , 'cornflowerblue'  , 'b'               ,'r'                , 'magenta'         ] ] )
        plt.savefig(savefilename+'.png',format='png')


