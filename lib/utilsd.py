#######################################
### DEEP LEARNING WITH TENSORFLOW 2 ###
### UTILITY FUNCTION DEFINITIONS    ###
### by: OAMEED NOAKOASTEEN          ###
#######################################

def getFILENAMES(PATH,COMPLETE=True):
 import os
 fn=[]
 for FILE in os.listdir(PATH):
  if COMPLETE:
   if not FILE.startswith('.'):
    fn.append(os.path.join(PATH,FILE))
  else:
   if not FILE.startswith('.'):
    fn.append(FILE)
 return fn

def wHDF(FILENAME,DIRNAMES,DATA):
 import h5py
 fobj=h5py.File(FILENAME,'w')
 for i in range(len(DIRNAMES)):
  fobj.create_dataset(DIRNAMES[i],data=DATA[i])
 fobj.close()

def download(URL,PATH):
 import requests
 with open(PATH,'wb') as fobj:
  fobj.write(requests.get(URL).content)

def clone(URL,PATH,NAME):
 import os
 import shutil
 os.system("git clone "+URL)
 _=shutil.move(NAME,os.path.join(PATH,NAME))
 
def mDIR(PATH):
 import os
 import shutil
 if os.path.exists(PATH):
  shutil.rmtree(PATH, ignore_errors=True)
 os.mkdir(PATH)

def rDIR(PATH):
 import os
 import shutil
 if os.path.exists(PATH):
  shutil.rmtree(PATH, ignore_errors=True)
 
def tar_uncompress(FILENAME,PATH):
 import tarfile
 with tarfile.open(FILENAME,'r') as tar:
  tar.extractall(path=PATH)

def pickle_read(FILENAME):
 import pickle
 with open(FILENAME,'rb') as pic:
  return pickle.load(pic, encoding='bytes')

def plotter(MODE,DATA,CONFIG):
 import numpy                  as np
 import tensorflow             as tf
 from matplotlib import pyplot as plt
 if   MODE=='collage':
  # CONFIG[0]: COLORMAP
  size=10
  fig =plt.figure()
  for  i in range(size):
   for j in range(size):
    ax=plt.subplot(size,size,i*size+(j+1))
    ax.imshow     (DATA[i*size+j], cmap=CONFIG[0])
    ax.axis       ('off')
  plt.subplots_adjust(left=0,right=1,bottom=0,top=1,wspace=0,hspace=0.075)
 else:
  if       MODE=='cm'     :
   import itertools
   # CONFIG[0]: CLASS NAMES
   cm =tf.math.confusion_matrix(DATA[0],DATA[1],dtype=tf.float32).numpy()
   fig=plt.figure(figsize=(8, 8))
   plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
   plt.title("Confusion Matrix")
   plt.colorbar()
   plt.xticks(np.arange(len(CONFIG[0])), CONFIG[0], rotation=45)
   plt.yticks(np.arange(len(CONFIG[0])), CONFIG[0]             )
   labels   =np.around(cm/cm.sum(axis=1)[:, np.newaxis], decimals=2)
   threshold=cm.max()/2.0
   for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
     color  = "white" if cm[i, j] > threshold else "black"
     plt.text(j, i, labels[i, j], horizontalalignment="center", color=color)
   plt.tight_layout()
   plt.ylabel('True Label')
   plt.xlabel('Predicted Label')
  else:
   if      MODE=='metrics':
    # CONFIG[0]: COLOR CODES
    idx=np.array([i/1e3 for i in range(len(DATA[1][0]))])
    fig,ax=plt.subplots()
    for i in range(len(DATA[0])):
     ax.plot(idx, DATA[1][i], CONFIG[0][i], linewidth=2, label=DATA[0][i])
    ax.set_title (' Training Metrics ')
    ax.set_xlabel(' Iterations (K) '  )
    ax.legend    ()
    ax.grid      ()
   else:
    if     MODE=='video':
     pred,data=DATA
     
     fig      =plt.figure(figsize=(20,10))
     for  i in range(data.shape[0]):
      for j in range(data.shape[1]):
       ax=plt.subplot(2*  data.shape[0]      ,
                          data.shape[1]      ,
                      2*i*data.shape[1]+(j+1) )
       ax.imshow     (pred[i,j], cmap='gray'  )
       ax.axis       ('off')
      for j in range(data.shape[1]):
       ax=plt.subplot(2*  data.shape[0]                    ,
                          data.shape[1]                    ,
                      2*i*data.shape[1]+(data.shape[1]+j+1) )
       ax.imshow     (data[i,j], cmap='viridis'             )
       ax.axis       ('off')
     plt.subplots_adjust(left=0,right=1,bottom=0,top=1,wspace=0,hspace=0.075)
    else:
     if    MODE=='txt':
      fig   =plt.figure()
      size  =plt.gcf().get_size_inches().tolist()
      fig.set_size_inches(2*size[0],0.5*size[1])
      ax=fig.add_subplot(3,1,1)
      plt.text (0,1   ,'PORTUGUESE'        +'\n'        , ha='left', weight='bold', wrap=True)
      plt.text (0,0.75, DATA[0]                         , ha='left',                wrap=True)
      ax.axis('off')
      ax=fig.add_subplot(3,1,2)
      plt.text (0,1   ,'ENGLISH'           +'\n'        , ha='left', weight='bold', wrap=True)
      plt.text (0,0.75, DATA[1]                         , ha='left',                wrap=True)
      ax.axis('off')
      if len(DATA)==3:
       ax=fig.add_subplot(3,1,3)
       plt.text (0,1   ,'ENGLISH PREDICTED'+'\n'        , ha='left', weight='bold', wrap=True)
       plt.text (0,0.75, DATA[2].numpy().decode('utf-8'), ha='left',                wrap=True)
       ax.axis('off')
     else:
      if   MODE=='posenc':
       fig =plt.figure()
       plt.pcolormesh(DATA.numpy().T, cmap='RdBu')
       plt.ylabel('Depth')
       plt.xlabel('Position')
       plt.colorbar()
      else:
       if  MODE=='attw':
        def plot_attention_head(in_tokens,translated_tokens,attention):
         translated_tokens=translated_tokens[1:]
         ax               =plt.gca()
         ax.matshow(attention)
         ax.set_xticks(range(len(in_tokens)))
         ax.set_yticks(range(len(translated_tokens)))
         labels           =[label.decode('utf-8') for label in in_tokens.numpy()        ]
         ax.set_xticklabels(labels, rotation=90)
         labels           =[label.decode('utf-8') for label in translated_tokens.numpy()]
         ax.set_yticklabels(labels)
        sentence         =DATA[0]
        translated_tokens=DATA[1]
        attention_heads  =DATA[2]
        in_tokens        =tf.convert_to_tensor([sentence])
        in_tokens        =CONFIG[0].pt.tokenize(in_tokens).to_tensor()
        in_tokens        =CONFIG[0].pt.lookup(in_tokens)[0]
        fig              =plt.figure(figsize=(16, 8))
        for h, head in enumerate(attention_heads):
         ax              =fig.add_subplot(2, 4, h+1)
         plot_attention_head(in_tokens, translated_tokens, head)
         ax.set_xlabel(f'Head {h+1}')
         plt.tight_layout()
       else:
        if MODE=='lr':
         import tensorflow as tf
         fig=plt.figure()
         plt.plot(DATA(tf.range(40000, dtype=tf.float32)))
         plt.ylabel("Learning Rate")
         plt.xlabel("Train Step"   )
 return fig

def plot_to_image(figure):
 import                               io
 import                 tensorflow as tf
 from matplotlib import pyplot     as plt
 buf  =io.BytesIO()
 plt.savefig(buf, format='png')
 plt.close  (figure) 
 buf.seek   (0)
 image=tf.image.decode_png(buf.getvalue(), channels=0)
 image=tf.expand_dims     (image         ,          0)
 return image

def get_transformer_tokenizers(PATH):
 import os
 import tensorflow      as tf
 import tensorflow_text as text
 model_name=os.path.join(PATH,"ted_hrlr_translate_pt_en_converter")
 tf.keras.utils.get_file(f"{model_name}.zip"                                                              ,
                         f"https://storage.googleapis.com/download.tensorflow.org/models/{model_name}.zip",
                         cache_dir   =PATH                                                                , 
                         cache_subdir=''                                                                  , 
                         extract     =True                                                                 )
 return tf.saved_model.load(model_name)


