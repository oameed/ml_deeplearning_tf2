#######################################
### DEEP LEARNING WITH TENSORFLOW 2 ###
### INPUT FUNCTION DEFINITIONS      ###
### by: OAMEED NOAKOASTEEN          ###
#######################################

import os
import numpy      as np
import tensorflow as tf
from utilsd   import (getFILENAMES              ,
                      get_transformer_tokenizers )
from datasetd import  dataset_get

def data_get_scale_cast(PATHS,PARAMS,MODE):
 if  PARAMS[0][2]==1:
  minmax =lambda X:  X/255
 else:
  if PARAMS[0][2]==2:
   minmax=lambda X: (X-127.5)/127.5
 img,lab =dataset_get(PARAMS[0][0],PATHS[0],MODE)
 img     =minmax(img).astype('float32')
 return img, lab

def saveTFRECORDS(PATHS, PARAMS, MODE):
 def get_train_validation_splits(IMG,LAB,SPLIT):
  rng       =np.random.default_rng()
  indeces   =[i for i in range(IMG.shape[0])]
  rng.shuffle(indeces)
  img       =IMG[indeces]
  lab       =LAB[indeces]
  img_splits=np.split(img,[int(img.shape[0]*(1-SPLIT))], axis=0)
  lab_splits=np.split(lab,[int(img.shape[0]*(1-SPLIT))], axis=0)
  return img_splits[0], lab_splits[0], img_splits[1], lab_splits[1]
 def serialize_example(IMG,LAB):  
  feature      ={'img'    : tf.train.Feature(bytes_list=tf.train.BytesList(value=[IMG.tostring()])), 
                 'lab'    : tf.train.Feature(int64_list=tf.train.Int64List(value=[LAB           ])) }
  example_proto=tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()
 def write_serialized_example(IMG,LAB,MODE):
  filename =os.path.join(PATHS[2],MODE,'batch'+'_'+str(1)+'.tfrecords')
  with tf.io.TFRecordWriter(filename) as writer:
   for i in range(IMG.shape[0]):
    example=serialize_example(IMG[i],LAB[i])
    writer.write(example)
 img,lab=data_get_scale_cast(PATHS,PARAMS,MODE)
 if  MODE=='train':
  img_train, lab_train, img_validation, lab_validation=get_train_validation_splits(img,lab,PARAMS[0][8])
  write_serialized_example (img_train     , lab_train     , 'train'     )
  write_serialized_example (img_validation, lab_validation, 'validation')
 else:
  if MODE=='test' :
   write_serialized_example(img           ,lab            , 'test'      )

def inputTFRECORDS(PATH, PARAMS):
 SHAPE     =PARAMS[2]
 filenames =getFILENAMES(PATH)
 feature   ={'img'    : tf.io.FixedLenFeature([],tf.string), 
             'lab'    : tf.io.FixedLenFeature([],tf.int64 ) }
 def parse_function(example_proto):
  parsed_example=tf.io.parse_single_example(example_proto,feature )
  _img          =tf.io.decode_raw(parsed_example['img'],tf.float32)
  _img.set_shape([np.prod(SHAPE)])
  img           =tf.reshape(_img,SHAPE)
  lab           =parsed_example['lab']
  return img,lab
 dataset =tf.data.TFRecordDataset(filenames                                    )
 dataset =dataset.map            (parse_function                               )
 dataset =dataset.batch          (PARAMS[0][3]  , drop_remainder          =True)
 dataset =dataset.shuffle        (PARAMS[0][5]  , reshuffle_each_iteration=True)
 return dataset

def input_PT_to_ENG(PARAMS,PATHS,MODE):
 def make_batches(DATASET,BATCHSIZE,BUFFERSIZE,PATHS):
  def tokenize_pairs(pt, en):
   MAX_TOKENS=128
   pt        =tokenizers.pt.tokenize(pt)
   pt        =pt[:,  :MAX_TOKENS  ]
   pt        =pt.to_tensor() 
   en        =tokenizers.en.tokenize(en)
   en        =en[:,  :MAX_TOKENS+1]
   en_inputs =en[:,  :-1].to_tensor()
   en_labels =en[:, 1:  ].to_tensor()
   return (pt, en_inputs), en_labels
  tokenizers=get_transformer_tokenizers(PATHS[0])
  dataset   =DATASET.cache             ()
  dataset   =dataset.shuffle           (BUFFERSIZE                                         )
  dataset   =dataset.batch             (BATCHSIZE                                          )
  dataset   =dataset.map               (tokenize_pairs, num_parallel_calls=tf.data.AUTOTUNE)
  dataset   =dataset.prefetch          (tf.data.AUTOTUNE                                   )
  return dataset
 examples,_=dataset_get (PARAMS[0][0],
                         []          ,
                         MODE         )
 batches   =make_batches(examples    ,
                         PARAMS[0][3],
                         PARAMS[0][5],
                         PATHS        )
 return batches


