#! /bin/bash

source  activate tf2py
cd      run/tensorflow

echo ' CREATING PROJECT DIRECTORY '                            'v31'
rm     -rf                                       ../../networks/v31
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v31

echo ' TRAINING ENCODER-RNN-DECODER ON MovingMNIST '
python train_rnn.py -data type4 -net v31 -b 128 -epc 2 -cpu
matlab -nodisplay -nosplash -nodesktop -r "graphics('training'  ,'../../networks/v31/logs/train' ,[3]);exit;"

echo ' GENERATING PREDICTIONS '
python predict.py   -data type4 -net v31 -pt 3
matlab -nodisplay -nosplash -nodesktop -r "graphics('prediction','../../networks/v31/predictions',[] );exit;"


