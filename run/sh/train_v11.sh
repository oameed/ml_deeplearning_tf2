#! /bin/bash

source  activate tf2py
cd      run/tensorflow

echo ' CREATING PROJECT DIRECTORIES '
rm     -rf                                       ../../networks/v11
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v11

echo ' TRAINING CLASSIFIER ON MNIST '
python train_classifier.py -data type1 -net v11 -b 128 -epc 2 -cpu

echo ' GENERATING PREDICTIONS '
python predict.py   -data type1 -net v11 -pt 1


