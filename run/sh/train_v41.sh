#! /bin/bash

source  activate tf2py
cd      run/tensorflow

echo ' CREATING PROJECT DIRECTORIES '
rm     -rf                                       ../../networks/v41
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v41

echo ' TRAINING GAN ON MNIST '
python train_gan.py -data type1 -net v41 -sc 2 -b 128 -epc 2 -cpu
matlab -nodisplay -nosplash -nodesktop -r "graphics('training'  ,'../../networks/v41/logs/train' ,[4]);exit;"

echo ' GENERATING PREDICTIONS '
python predict.py   -data type1 -net v41 -pt 4


