#! /bin/bash

source  activate tf211py
cd      run/tensorflow

echo ' CREATING PROJECT DIRECTORIES '
rm     -rf                                       ../../networks/v51
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v51

echo ' TRAINING TRANSFORMER ON PORTUGUESE-ENGLISH TRANSLATION DATASET '
python train_transformer.py -data type5 -net v51 -b 64 -epc 2 -bc 20000

echo ' GENERATING PREDICTIONS '
python predict.py   -data type5 -net v51 -pt 5


