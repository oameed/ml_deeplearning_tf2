#! /bin/bash

echo   ' ACTIVATING TENSORFLOW ENVIRONMENT '
source  activate tf2py

cd run/tensorflow

echo ' SAMPLING MNIST        DATASET '
python sampledata.py -data type1

echo ' SAMPLING FashionMNIST DATASET '
python sampledata.py -data type2

echo ' SAMPLING CIFAR10      DATASET '
python sampledata.py -data type3

echo ' SAMPLING MovingMNIST  DATASET '
python sampledata.py -data type4
matlab -nodisplay -nosplash -nodesktop -r "graphics('sample','../../data/movingmnist/samples',[]);exit;"

conda   deactivate 
source  activate   tftxt

echo ' SAMPLING  PORTUGUESE-ENGLISH TRANSLATION DATASET '
python sampledata.py -data type5


