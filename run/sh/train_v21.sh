#! /bin/bash

source  activate tf2py
cd      run/tensorflow

echo ' CREATING PROJECT DIRECTORIES '
rm     -rf                                       ../../networks/v21
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v21

echo ' TRAINING AUTOENCODER ON MNIST '
python train_ae.py -data type1 -net v21 -b 128 -epc 2 -cpu

echo ' GENERATING PREDICTIONS '
python predict.py   -data type1 -net v21 -pt 2


