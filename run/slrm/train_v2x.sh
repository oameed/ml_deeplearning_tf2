#! /bin/bash

#SBATCH --ntasks=8
#SBATCH --time=48:00:00
#SBATCH --partition=singleGPU
#SBATCH --gres=gpu:1
#SBATCH --mail-type=BEGIN,FALI,END
#SBATCH --mail-user=
#SBATCH --job-name=AE

module load anaconda3

source  activate tf2py
cd      $SLURM_SUBMIT_DIR
cd      ../tensorflow


echo ' CREATING PROJECT DIRECTORY FOR '                        'v21'
rm     -rf                                       ../../networks/v21
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v21
echo ' TRAINING AUTOENCODER ON MNIST '
python train_ae.py -data type1 -net v21 -b 128 -epc 15
echo ' GENERATING PREDICTIONS '
python predict.py  -data type1 -net v21 -pt 2


echo ' CREATING PROJECT DIRECTORY FOR '                        'v22'
rm     -rf                                       ../../networks/v22
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v22
echo ' TRAINING AUTOENCODER ON FashionMNIST '
python train_ae.py -data type2 -net v22 -b 128 -epc 15
echo ' GENERATING PREDICTIONS '
python predict.py  -data type2 -net v22 -pt 2


echo ' CREATING PROJECT DIRECTORY FOR '                        'v23'
rm     -rf                                       ../../networks/v23
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v23
echo ' TRAINING AUTOENCODER ON CIFAR-10 '
python train_ae.py -data type3 -net v23 -b 128 -epc 30
echo ' GENERATING PREDICTIONS '
python predict.py  -data type3 -net v23 -pt 2


