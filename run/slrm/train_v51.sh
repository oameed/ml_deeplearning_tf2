#! /bin/bash

#SBATCH --ntasks=1
#SBATCH --time=48:00:00
#SBATCH --partition=singleGPU
#SBATCH --gres=gpu:1
#SBATCH --mail-type=BEGIN,FAIL,END
#SBATCH --mail-user=
#SBATCH --job-name=Transformer

module load miniconda3


source  activate tf211py
cd      $SLURM_SUBMIT_DIR
cd      ../tensorflow

echo ' CREATING PROJECT DIRECTORIES '
rm     -rf                                       ../../networks/v51
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v51
echo ' TRAINING TRANSFORMER ON PORTUGUESE-ENGLISH TRANSLATION DATASET '
python train_transformer.py -data type5 -net v51 -b 64 -epc 20 -bc 1000
echo ' GENERATING PREDICTIONS '
python predict.py           -data type5 -net v51 -pt 5


