#! /bin/bash

#SBATCH --ntasks=8
#SBATCH --time=48:00:00
#SBATCH --partition=singleGPU
#SBATCH --gres=gpu:1
#SBATCH --mail-type=BEGIN,FALI,END
#SBATCH --mail-user=
#SBATCH --job-name=Classifier

module load anaconda3

source  activate tf2py
cd      $SLURM_SUBMIT_DIR
cd      ../tensorflow


echo ' CREATING PROJECT DIRECTORY FOR '                        'v11'
rm     -rf                                       ../../networks/v11
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v11
echo ' TRAINING CLASSIFIER ON MNIST '
python train_classifier.py -data type1 -net v11 -b 128 -epc 10
echo ' GENERATING PREDICTIONS '
python predict.py          -data type1 -net v11 -pt 1


echo ' CREATING PROJECT DIRECTORY FOR '                        'v12'
rm     -rf                                       ../../networks/v12
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v12
echo ' TRAINING CLASSIFIER ON FashioinMNIST '
python train_classifier.py -data type2 -net v12 -b 128 -epc 10
echo ' GENERATING PREDICTIONS '
python predict.py          -data type2 -net v12 -pt 1


echo ' CREATING PROJECT DIRECTORY FOR '                        'v13'
rm     -rf                                       ../../networks/v13
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v13
echo ' TRAINING CLASSIFIER ON CIFAR-10 '
python train_classifier.py -data type3 -net v13 -b 128 -epc 20
echo ' GENERATING PREDICTIONS '
python predict.py          -data type3 -net v13 -pt 1


