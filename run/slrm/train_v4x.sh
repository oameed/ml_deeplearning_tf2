#! /bin/bash

#SBATCH --ntasks=8
#SBATCH --time=48:00:00
#SBATCH --partition=singleGPU
#SBATCH --gres=gpu:1
#SBATCH --mail-type=BEGIN,FALI,END
#SBATCH --mail-user=
#SBATCH --job-name=GAN

module load anaconda3
module load matlab

source  activate tf2py
cd      $SLURM_SUBMIT_DIR
cd      ../tensorflow


echo ' CREATING PROJECT DIRECTORY FOR '                        'v41'
rm     -rf                                       ../../networks/v41
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v41
echo ' TRAINING GAN ON MNIST '
python train_gan.py -data type1 -net v41 -sc 2 -b 128 -epc 50
matlab -nodisplay -nosplash -nodesktop -r "graphics('training'  ,'../../networks/v41/logs/train' ,[4]);exit;"
echo ' GENERATING PREDICTIONS '
python predict.py   -data type1 -net v41 -pt 4


echo ' CREATING PROJECT DIRECTORY FOR '                        'v42'
rm     -rf                                       ../../networks/v42
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v42
echo ' TRAINING GAN ON FashionMNIST '
python train_gan.py -data type2 -net v42 -sc 2 -b 128 -epc 50
matlab -nodisplay -nosplash -nodesktop -r "graphics('training'  ,'../../networks/v42/logs/train' ,[4]);exit;"
echo ' GENERATING PREDICTIONS '
python predict.py   -data type2 -net v42 -pt 4


echo ' CREATING PROJECT DIRECTORY FOR '                        'v43'
rm     -rf                                       ../../networks/v43
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v43
echo ' TRAINING GAN ON CIFAR-10 '
python train_gan.py -data type3 -net v43 -sc 2 -b 128 -epc 200
matlab -nodisplay -nosplash -nodesktop -r "graphics('training'  ,'../../networks/v43/logs/train' ,[4]);exit;"
echo ' GENERATING PREDICTIONS '
python predict.py   -data type3 -net v43 -pt 4


