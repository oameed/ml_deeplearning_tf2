%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH TENSORFLOW 2 %%%
%%% GENERATING GIFS FOR VIDEOS      %%%
%%% by: OAMEED NOAKOASTEEN          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% TYPE  : PROBLEM TYPE: 'sample' / 'prediction' / 'training'
% CONFIG: CONFIGURATION FOR 'training' MODE: '3' / '4'

function graphics(TYPE,PATH,CONFIG)
close all
clc

if strcmp(TYPE,'sample')
    [rFILENAME,dFILENAME]=getFILENAMES(PATH)                             ;
    if ~ size(dFILENAME,2)==0
        for i=1:size(dFILENAME,2)
            delete (fullfile(PATH,dFILENAME{i}))
        end
    end
    DIRLIST              =read_h5py_info(fullfile(PATH,rFILENAME{1}))    ;
    for i=1:size(DIRLIST,2)
        saveFILENAME     =fullfile(PATH,[TYPE,'_',DIRLIST{i},'.gif']);
        img              =h5read(fullfile(PATH,rFILENAME{1}),...
                                 fullfile('/' ,DIRLIST{i}  )    )        ;
        img              =permute(img,[4,3,2,1])                         ;
        make_gif(saveFILENAME,img)
    end
    delete (fullfile(PATH,rFILENAME{1}))
else
    if strcmp(TYPE,'prediction')
        [rFILENAME,dFILENAME]=getFILENAMES(PATH)                         ;
        if ~ size(dFILENAME,2)==0
            for i=1:size(dFILENAME,2)
                delete (fullfile(PATH,dFILENAME{i}))
            end
        end
        DIRLIST              =read_h5py_info(fullfile(PATH,rFILENAME{1}));
        videos               =h5read(fullfile(PATH,rFILENAME{1}),...
                                     fullfile('/' ,DIRLIST{1}  )    )    ;
        videos               =permute(videos,[4,3,2,1])                  ;
        for i=1:size(videos,1)
            saveFILENAME     =fullfile(PATH,[TYPE,'_',num2str(i),'.gif']);
            make_gif(saveFILENAME,squeeze(videos(i,:,:,:)))
        end
    else
        if strcmp(TYPE,'training')
            NAME      ='training'                                        ;
            rFILENAME =strcat(NAME,'.h5' )                               ;
            sFILENAME =strcat(NAME,'.gif')                               ;
            DIR       ='samples'                                         ;
            videos    =h5read(fullfile(PATH,rFILENAME),fullfile('/',DIR));
            videos    =permute(videos,[5,4,3,2,1])                       ;
            if CONFIG(1)==3
                DIR   ='true'                                            ;
                vt    =h5read(fullfile(PATH,rFILENAME),fullfile('/',DIR));
                vt    =permute(vt,[4,3,2,1])                             ;
                videos={videos,vt}                                       ;
            end
            make_gif_training(fullfile(PATH,sFILENAME),videos,CONFIG)
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FUNCTION DEFINITIONS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [H5,GIF]=getFILENAMES(PATH)
        H5       ={}                                                 ; 
        GIF      ={}                                                 ;
        dirlist  =dir(PATH)                                          ;
        NAMESLIST=string({dirlist(4:end).name})                      ;
        for index=1:size(NAMESLIST,2)
            splits=split(NAMESLIST{index},'.')                       ;
            if strcmp(splits(2),'h5')
                H5{end+1}=NAMESLIST(index)                           ;
            else
                if strcmp(splits(2),'gif')
                    GIF{end+1}=NAMESLIST(index)                      ;
                end
            end
        end
    end

    function dirlist=read_h5py_info(FILENAME)
        info          =h5info(FILENAME)                            ;
        dirlist       ={}                                          ;
        for index=1:size(info.Datasets,1)            
            dirlist{index}=info.Datasets(index).Name               ;
        end
    end

    function plotter(DATA)
        imshow(DATA)
        set  (gca,'units'   ,'pixels'             ) 
        x=get(gca,'position'                      )                ;
        set  (gcf,'units'   ,'pixels'             ) 
        y=get(gcf,'position'                      )                ;
        set  (gcf,'position',[y(1) y(2) x(3) x(4)])
        set  (gca,'units'   ,'normalized','position',[0 0 1 1])
    end

    function make_gif(FILENAME,IMG)
        PAUSE=0.50                                                 ;
        for index=1:size(IMG,1)
            plotter(squeeze(IMG(index,:,:)))            
            [imind,cm]=rgb2ind(frame2im(getframe(gcf)),256)        ;
            if index==1
                imwrite(imind,cm,FILENAME,'gif','Loopcount',inf     ,'DelayTime',PAUSE)
            else
                imwrite(imind,cm,FILENAME,'gif','WriteMode','append','DelayTime',PAUSE)
            end
        end
    end

    function plotter_training(DATA,TRUE,CONFIG)
        if CONFIG(1)==3
            for frame=1:size(DATA,1)
                subplot('Position',[(frame-1)*(1/20) 0   (1/20)*(9/10) (0.5)*(9/10)])
                imshow (squeeze(DATA(frame,:,:)))
                subplot('Position',[(frame-1)*(1/20) 0.5 (1/20)*(9/10) (0.5)*(9/10)])
                imshow (squeeze(TRUE(frame,:,:)))
            end
        else
            if CONFIG(1)==4
                for row=1:10
                    for col=1:10
                        subplot('Position',[(row-1)*(1/10) (col-1)*(1/10) (9/10)*(1/10) (9/10)*(1/10)])
                        imshow(squeeze(DATA((row-1)*10+col,:,:,:)))
                    end
                end                    
            end
        end
    end

    function make_gif_training(FILENAME,VIDS,CONFIG)
        PAUSE=0.50                                                ;
        figure
        if CONFIG(1)==3
            IMG=VIDS{1};
            TRU=VIDS{2};
            x  =get(gcf,'position');
            set  (gcf,'position',[x(1),x(2),20*128,128])
        else
            if CONFIG(1)==4
                IMG=VIDS;
                TRU=[]  ;
                x  =get(gcf,'position');
                set  (gcf,'position',[x(1),x(2),x(3),x(3)])
            end
        end
        for index=1:size(IMG,1)
            plotter_training(squeeze(IMG(index,:,:,:,:)),TRU,CONFIG)            
            [imind,cm]=rgb2ind(frame2im(getframe(gcf)),256)        ;
            if index==1
                imwrite(imind,cm,FILENAME,'gif','Loopcount',inf     ,'DelayTime',PAUSE)
            else
                imwrite(imind,cm,FILENAME,'gif','WriteMode','append','DelayTime',PAUSE)
            end
        end
    end

end

