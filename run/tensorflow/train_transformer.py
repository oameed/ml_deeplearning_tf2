######################################################
### DEEP LEARNING WITH TENSORFLOW 2                ###
### TRAINING TRANSFORMER                           ###
### by: OAMEED NOAKOASTEEN                         ###
######################################################

import                             os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import absl.logging
absl.logging.set_verbosity(absl.logging.ERROR)
import                             sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
import numpy                    as np
import tensorflow               as tf
from   matplotlib import pyplot as plt
from   paramd     import           PATHS, PARAMS 
from   inputd     import           input_PT_to_ENG
from   utilsd     import          (plotter                ,
                                   plot_to_image          ,
                                   wHDF                    )
from   nndTRNSF   import          (positional_encoding    ,
                                   PositionalEmbedding    ,
                                   CrossAttention         ,
                                   GlobalSelfAttention    ,
                                   CausalSelfAttention    ,
                                   FeedForward            ,
                                   EncoderLayer           ,
                                   DecoderLayer           ,
                                   Encoder                ,
                                   Decoder                ,
                                   Transformer            ,
                                   CustomSchedule         ,
                                   masked_loss            ,
                                   masked_accuracy        ,
                                   callback_custom_ckpt   ,
                                   callback_custom_history )

tfr_train     =os.path.join    ( PATHS[2] ,'train'       )
tfr_validation=os.path.join    ( PATHS[2] ,'validation'  )
log_train     =os.path.join    ( PATHS[4] ,'train'       )
pe_path       =os.path.join    (*PATHS[2].split('/')[:-1])

#############
### MODEL ###
#############
model         =Transformer(num_layers         =PARAMS[5][0]       ,
                           d_model            =PARAMS[5][1]       ,
                           num_heads          =PARAMS[5][3]       ,
                           dff                =PARAMS[5][2]       ,
                           input_vocab_size   =PARAMS[5][5]       ,
                           target_vocab_size  =PARAMS[5][6]       ,
                           POSITIONAL_ENCODING=positional_encoding,
                           POSITIONALEMBEDDING=PositionalEmbedding,
                           CROSSATTENTION     =CrossAttention     ,
                           GLOBALSELFATTENTION=GlobalSelfAttention,
                           CAUSALSELFATTENTION=CausalSelfAttention,
                           FEEDFORWARD        =FeedForward        , 
                           ENCODERLAYER       =EncoderLayer       ,
                           DECODERLAYER       =DecoderLayer       ,
                           ENCODER            =Encoder            ,
                           DECODER            =Decoder            ,
                           paths              =PATHS              ,
                           dropout_rate       =PARAMS[5][4]        )

learning_rate =CustomSchedule(PARAMS[5][1])

model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate,
                                                 beta_1 =0.9  ,
                                                 beta_2 =0.9  ,
                                                 epsilon=1e-9  ),
              loss     = masked_loss                            ,
              accuracy = masked_accuracy                         )

plotter    ('posenc'                               ,
             positional_encoding(2048,PARAMS[5][1]),
             []                                     )

plt.savefig(os.path.join(pe_path,'positional_encoding'+'.png'), 
            format='png'                                       )

plotter    ('lr'         ,
            learning_rate,
            []            )

plt.savefig(os.path.join(pe_path,'learning_rate'+'.png'), 
            format='png'                                 )

#################
### CALLBACKS ###
#################
callbacks_train=[callback_custom_ckpt          ()                         ,
                 callback_custom_history       (plotter       =plotter  ,
                                                whdf          =wHDF      ),
                 tf.keras.callbacks.TensorBoard(log_dir       =log_train, 
                                                histogram_freq=1        , 
                                                update_freq   ='batch'   ) ]

#############
### TRAIN ###
#############
print        (' FITTING  MODEL'                           )
train_batches=input_PT_to_ENG(PARAMS,PATHS,'train'        )

model.fit    (x                    =train_batches  ,
              epochs               =PARAMS[0][4]   ,  
              callbacks            =callbacks_train,
              verbose              =1                     )

print        (' TRAINING FINISHED '                       )


