#######################################
### DEEP LEARNING WITH TENSORFLOW 2 ###
### DATASET SAMPLING                ###
### by: OAMEED NOAKOASTEEN          ###
#######################################

import os
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
from paramd   import PATHS, PARAMS 
from datasetd import dataset_sample

dataset_sample(PATHS,PARAMS)


