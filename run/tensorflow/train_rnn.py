###############################################
### DEEP LEARNING WITH TENSORFLOW 2         ###
### TRAIN ENCODER-RECURRENT-DECODER NETWORK ###
### by: OAMEED NOAKOASTEEN                  ###
###############################################

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
import numpy             as np
import tensorflow        as tf
from   paramd     import    PATHS, PARAMS 
from   inputd     import   (data_get_scale_cast    ,
                            saveTFRECORDS          , 
                            inputTFRECORDS          )
from   utilsd     import   (plotter                ,
                            plot_to_image          ,
                            wHDF                    )
from   nndRNN     import   (callback_custom_ckpt   ,
                            callback_custom_monitor,
                            callback_custom_history,
                            get_encoder            ,
                            VanillaRNNCell         ,
                            get_decoder            ,
                            loss                   ,
                            ERD                     )

tfr_train       =os.path.join(PATHS[2],'train'     )
tfr_validation  =os.path.join(PATHS[2],'validation')
log_train       =os.path.join(PATHS[4],'train'     )
DEVICE          =PARAMS[0][10]+':'+'0'

monitor_data, _ =data_get_scale_cast(PATHS,PARAMS,'test')
total           =monitor_data.shape[0]
idx             =np.random.choice   ([i for i in range(total)], size=10, replace=False  ).tolist()
monitor_data    =np.array           ([monitor_data[i] for i in range(total) if i in idx])

#############
### MODEL ###
#############

tf.config.set_soft_device_placement(True)

with tf.device(DEVICE):
 model           =ERD(encoder= get_encoder         (          PARAMS[2][1:]         ),
                      rnn    = tf.keras.layers.LSTM( 8*8*32 , return_sequences=True ),
                      decoder= get_decoder         ([8,8,32], PARAMS[2][-1]         ),
                      paths  = PATHS                                                 ,
                      params = PARAMS                                                 )

 model.compile(optimizer     = tf.keras.optimizers.Adam(),
               loss          = loss                       )
              
# THE CUSTOM DEFINED 'VanillaRNNCell' CAN BE USED AS:
# RNN= tf.keras.layers.RNN(VanillaRNNCell(256), return_sequences=True)

#################
### CALLBACKS ###
#################

callbacks_train=[callback_custom_ckpt          ()                                                       ,
                 callback_custom_monitor       (writer        =tf.summary.create_file_writer(log_train),
                                                data          =monitor_data                            ,
                                                plotter       =plotter                                 ,
                                                converter     =plot_to_image                            ),
                 callback_custom_history       (plotter       =plotter                                 ,
                                                whdf          =wHDF                                     ),
                 tf.keras.callbacks.TensorBoard(log_dir       =log_train                               , 
                                                histogram_freq=1                                       , 
                                                update_freq   ='batch'                                  ) ]

#############
### TRAIN ###
#############
print        (' WRITING  TRAINING   DATA TO TFRECORDS FORMAT ')
saveTFRECORDS(PATHS, PARAMS, 'train')

print        (' FITTING  MODEL'                     )
data_train     =inputTFRECORDS(tfr_train     ,PARAMS)
data_validation=inputTFRECORDS(tfr_validation,PARAMS)

model.fit    (x                    =data_train     ,
              epochs               =PARAMS[0][4]   ,
              validation_data      =data_validation,
              callbacks            =callbacks_train,
              verbose              =1               )
print        (' TRAINING FINISHED '                 )


