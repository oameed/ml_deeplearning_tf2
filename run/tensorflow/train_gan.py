######################################################
### DEEP LEARNING WITH TENSORFLOW 2                ###
### TRAINING GENERATIVE ADVERSESRIAL NETWORK (GAN) ###
### by: OAMEED NOAKOASTEEN                         ###
######################################################

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
import numpy             as np
import tensorflow        as tf
from   paramd     import    PATHS, PARAMS 
from   inputd     import   (saveTFRECORDS          , 
                            inputTFRECORDS          )
from   utilsd     import   (plotter                ,
                            plot_to_image          ,
                            wHDF                    )
from   nndGAN     import   (callback_custom_ckpt   ,
                            callback_custom_monitor,
                            callback_custom_history,
                            get_discriminator      ,
                            get_generator          ,
                            z_sample               ,
                            loss_wasserstein_disc  ,
                            loss_wasserstein_genr  ,
                            GAN                     )

tfr_train     =os.path.join    (PATHS[2] ,'train'      )
tfr_validation=os.path.join    (PATHS[2] ,'validation' )
log_train     =os.path.join    (PATHS[4] ,'train'      )
DEVICE        =PARAMS[0][10]+':'+'0'

Z             =z_sample        ([     100,PARAMS[0][7]])
Z_for_monitor =np.random.normal(size=(100,PARAMS[0][7]))
size_reduced  =int(PARAMS[2][0]/4)

#############
### MODEL ###
#############

tf.config.set_soft_device_placement(True)

with tf.device(DEVICE):
 model        =GAN(discriminator= get_discriminator(PARAMS[2]                                                   ),
                   generator    = get_generator    ([size_reduced,size_reduced,128], PARAMS[0][7], PARAMS[2][-1]),
                   paths        = PATHS                                                                          , 
                   params       = PARAMS                                                                          )

 model.compile(d_optimizer      = tf.keras.optimizers.Adam(learning_rate=1e-4), # tf.keras.optimizers.RMSprop(learning_rate=5e-5) 
               g_optimizer      = tf.keras.optimizers.Adam(learning_rate=1e-4), # tf.keras.optimizers.RMSprop(learning_rate=5e-5) 
               loss_disc        = loss_wasserstein_disc                       ,
               loss_genr        = loss_wasserstein_genr                        )

#################
### CALLBACKS ###
#################
callbacks_train=[callback_custom_ckpt          ()                                                   ,
                 callback_custom_monitor       (writer   =tf.summary.create_file_writer(log_train),
                                                data     =Z_for_monitor                           ,
                                                plotter  =plotter                                 ,
                                                converter=plot_to_image                            ),
                 callback_custom_history       (plotter  =plotter                                 ,
                                                whdf     =wHDF                                     ),
                 tf.keras.callbacks.TensorBoard(log_dir       =log_train                            , 
                                                histogram_freq=1                                    , 
                                                update_freq   ='batch'                               )]

#############
### TRAIN ###
#############
print        (' WRITING  TRAINING   DATA TO TFRECORDS FORMAT ')
saveTFRECORDS(PATHS, PARAMS, 'train')

print        (' FITTING  MODEL'                               )
data_train     =inputTFRECORDS(tfr_train,PARAMS)
data_validation=              (Z        ,Z     )
model.fit    (x                    =data_train     ,
              epochs               =PARAMS[0][4]   ,
              validation_data      =data_validation,
              validation_batch_size=100            ,              
              callbacks            =callbacks_train,
              verbose              =1                     )
print        (' TRAINING FINISHED '                       )


