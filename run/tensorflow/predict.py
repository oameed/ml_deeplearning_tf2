####################################################
### DEEP LEARNING WITH TENSORFLOW 2              ###
### GENERATING PREDICTIONS USING A TRAINED MODEL ###
### by: OAMEED NOAKOASTEEN                       ###
####################################################

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
import numpy                    as  np
import tensorflow               as  tf
from   matplotlib import pyplot as  plt
from   paramd     import            PATHS, PARAMS
from   utilsd     import           (plotter, 
                                    wHDF    )
from   inputd     import            data_get_scale_cast

savefilename=os.path.join          (PATHS[5],'predictions' )

if     PARAMS[0][0] in ['type1','type2']:
 colormap   ='gray'
else:
 if    PARAMS[0][0] in ['type3'        ]:
  colormap  ='viridis'

if     PARAMS[0][9] in [1]:
 model   =tf.keras.models.load_model(PATHS[3]                 ) 
 img,lab =data_get_scale_cast(PATHS,PARAMS,'test'             )
 x       =model              (img)
 x       =tf.keras.layers.Softmax()(x)
 x       =np.array           ([np.argmax(i) for i in x]       )
 fig     =plotter            ('cm', [lab,x], [PARAMS[4]]      )
 plt.savefig                 (savefilename+'.png',format='png')
else:
 if    PARAMS[0][9] in [2]:
  model  =tf.keras.models.load_model(PATHS[3]                 ) 
  img,lab=data_get_scale_cast(PATHS,PARAMS,'test'             )
  idx    =[i for i in range(img.shape[0])]
  idx    =np.random.choice   (idx, size=100, replace=False    )
  img    =img                [idx]
  x      =model              (img)
  fig    =plotter            ('collage', x, [colormap]        )
  plt.savefig                (savefilename+'.png',format='png')
 else:
  if   PARAMS[0][9] in [3]:
   from nndRNN import VanillaRNNCell
   model    =tf.keras.models.load_model(PATHS[3], custom_objects={"VanillaRNNCell": VanillaRNNCell}) 
   videos, _=data_get_scale_cast(PATHS,PARAMS,'test')
   total    =videos.shape[0]
   idx      =np.random.choice   ([i for i in range(total)], size=10, replace=False).tolist()
   videos   =np.array           ([videos[i] for i in range(total) if i in idx]     )
   x        =model(videos)
   vids     =np.concatenate((videos,x), axis=2)
   vids     =np.squeeze(vids)
   wHDF(savefilename+'.h5',
        ['videos']        ,
        [vids    ]         )
  else:
   if  PARAMS[0][9] in [4]:
    model=tf.keras.models.load_model(PATHS[3]                 ) 
    Z    =np.random.normal   (size=(100,PARAMS[0][7])         )
    x    =model              (Z)
    fig  =plotter            ('collage', x, [colormap]        )
    plt.savefig              (savefilename+'.png',format='png')
   else:
    if PARAMS[0][9] in [5]:
     from utilsd   import get_transformer_tokenizers
     from nndTRNSF import (CustomSchedule,
                           Translator     )
     inputs    =[("este é um problema que temos que resolver."      ,
                  "this is a problem we have to solve ."             ),
                 ("os meus vizinhos ouviram sobre esta ideia."      ,
                  "and my neighboring homes heard about this idea ." ),
                 ("vou então muito rapidamente partilhar convosco algumas histórias de algumas coisas mágicas que aconteceram.",
                  "so i \'ll just share with you some stories very quickly of some magical things that have happened ."         ),
                 ("este é o primeiro livro que eu fiz."             ,
                  "this is the first book i've ever done."           )                                                            ]
     model     =tf.keras.models.load_model(PATHS[3], custom_objects={"CustomSchedule":CustomSchedule}) 
     translator=Translator                (get_transformer_tokenizers(PATHS[0]),
                                           model                                )
     for i in range(len(inputs )):
      sentence, ground_truth                               =inputs[i]
      translated_text, translated_tokens, attention_weights=translator(tf.constant(sentence))
      fig=plotter('txt' , (sentence,ground_truth,translated_text),[])
      plt.savefig(savefilename+'_'+str(i+1)+'_'+'txt' +'.png', format='png', transparent=True)
      plt.clf()
      fig=plotter('attw', (sentence, translated_tokens, tf.squeeze(attention_weights, 0)), [get_transformer_tokenizers(PATHS[0])])
      plt.savefig(savefilename+'_'+str(i+1)+'_'+'attw'+'.png', format='png', transparent=True)


